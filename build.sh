#!/usr/bin/env bash

# This script builds your Go project for different operating systems and architectures.
#
# Flags:
#   -l, --linux       : builds for Linux (interactive if no other flags given)
#   -w, --windows     : builds for Windows (interactive if no other flags given)
#   -m, --mac         : builds for macOS   (interactive if no other flags given)
#   -a, --all         : builds ALL combinations of OS and architectures (no interaction)
#   --arch=<arch>     : specifies the architecture (e.g. amd64, arm64, arm)
#
# Without flags, it will ask interactively:
# 1) Which OS to build for? (default: Windows)
# 2) Which architecture? (default: amd64)
#
# Pressing Enter without typing a number chooses the default.
# Thus, pressing Enter twice builds for Windows/amd64.
#
# If -a is chosen, it ignores other flags and builds all combos:
# OS: linux, windows, darwin
# ARCH: amd64, arm64, arm
#
# Note: darwin/arm is not supported, so we skip that combo.

set -e

# Color definitions
RED='\033[0;31m'
GREEN='\033[0;32m'
YELLOW='\033[0;33m'
BLUE='\033[0;34m'
CYAN='\033[0;36m'
BOLD='\033[1m'
RESET='\033[0m'

PROJECT_NAME="divekit"
CMD_PATH="."
BIN_DIR="bin"

DEFAULT_ARCH="amd64"

OS_LIST=("linux" "windows" "darwin")
ARCH_LIST=("amd64" "arm64" "arm")

show_usage_and_exit() {
    echo -e "${BOLD}Usage:${RESET} $0 [options]"
    echo "Options:"
    echo -e "  ${GREEN}-l, --linux${RESET}       Build for Linux"
    echo -e "  ${GREEN}-w, --windows${RESET}     Build for Windows"
    echo -e "  ${GREEN}-m, --mac${RESET}         Build for macOS"
    echo -e "  ${GREEN}-a, --all${RESET}         Build for ALL OS/ARCH combinations"
    echo -e "  ${GREEN}--arch=<arch>${RESET}      Specify architecture (e.g. amd64, arm64, arm)"
    exit 1
}

build_combo() {
    local os="$1"
    local arch="$2"
    local bin_name="${PROJECT_NAME}_${os}_${arch}"
    local ext=""

    # Skip unsupported combos
    if [ "$os" = "darwin" ] && [ "$arch" = "arm" ]; then
        echo -e "${RED}Skipping unsupported combination: $os/$arch${RESET}"
        return
    fi

    if [ "$os" = "windows" ]; then
        ext=".exe"
    fi

    local goos="$os"
    if [ "$os" = "mac" ]; then
        goos="darwin"
        bin_name="${PROJECT_NAME}_darwin_${arch}"
    fi
    if [ "$os" = "darwin" ]; then
        bin_name="${PROJECT_NAME}_darwin_${arch}"
    fi

    echo
    echo -e "${CYAN}Building for $os ($arch)...${RESET}"
    mkdir -p "$BIN_DIR"
    GOOS="$goos" GOARCH="$arch" go build -o "$BIN_DIR/${bin_name}${ext}" "$CMD_PATH"
    echo -e "  -> ${BLUE}$BIN_DIR/${bin_name}${ext}${RESET}"
}

build_all_combos() {
    for os in "${OS_LIST[@]}"; do
        for arch in "${ARCH_LIST[@]}"; do
            build_combo "$os" "$arch"
        done
    done
}

# Check flags
LINUX_FLAG=false
WINDOWS_FLAG=false
MAC_FLAG=false
ALL_FLAG=false
INTERACTIVE=false
ARCH_FLAG=""

# Parse arguments
while [[ $# -gt 0 ]]; do
    case $1 in
        -l|--linux)
            LINUX_FLAG=true
            shift
            ;;
        -w|--windows)
            WINDOWS_FLAG=true
            shift
            ;;
        -m|--mac)
            MAC_FLAG=true
            shift
            ;;
        -a|--all)
            ALL_FLAG=true
            shift
            ;;
        --arch=*)
            ARCH_FLAG="${1#--arch=}"
            shift
            ;;
        *)
            echo -e "${RED}Unknown argument:${RESET} $1"
            show_usage_and_exit
            ;;
    esac
done

# If ALL_FLAG is true, build all combos and exit directly
if [ "$ALL_FLAG" = true ]; then
    echo -e "${YELLOW}Building ALL OS/ARCH combinations...${RESET}"
    build_all_combos
    exit 0
fi

# If no OS flags given, go interactive for OS
if [ "$LINUX_FLAG" = false ] && [ "$WINDOWS_FLAG" = false ] && [ "$MAC_FLAG" = false ]; then
    INTERACTIVE=true
fi

if [ -z "$ARCH_FLAG" ]; then
    # No arch given, we may need to go interactive for architecture
    INTERACTIVE_ARCH=true
else
    INTERACTIVE_ARCH=false
fi

CHOSEN_OS=""
if [ "$INTERACTIVE" = true ]; then
    echo -e "To build for a specific OS, use flags like ${GREEN}-l${RESET}, ${GREEN}-w${RESET}, ${GREEN}-m${RESET}"
    echo -e "To build for a specific architecture, use ${GREEN}--arch=<arch>${RESET}"
    echo -e "To build for all OS/ARCH combinations, use ${GREEN}-a${RESET}"
    echo
    echo -e "${YELLOW}Which system do you want to build for?${RESET}"
    echo -e "  1  ${GREEN}Linux${RESET}"
    echo -e " [2] ${GREEN}Windows${RESET}"
    echo -e "  3  ${GREEN}macOS${RESET}"
    read -p "Pick 1-3 or press [Enter]: " choice_os

    if [ -z "$choice_os" ]; then
        choice_os="2"
    fi

    case $choice_os in
        1) CHOSEN_OS="linux" ;;
        2) CHOSEN_OS="windows" ;;
        3) CHOSEN_OS="darwin" ;; # intern nennen wir macOS hier darwin
        *)
            echo -e "${RED}Invalid choice.${RESET}"
            exit 1
            ;;
    esac
else
    # Non-interactive mode: determine chosen OS from flags
    if [ "$LINUX_FLAG" = true ]; then
        CHOSEN_OS="linux"
    elif [ "$WINDOWS_FLAG" = true ]; then
        CHOSEN_OS="windows"
    elif [ "$MAC_FLAG" = true ]; then
        CHOSEN_OS="darwin"
    fi
fi

if [ "$INTERACTIVE_ARCH" = true ]; then
    echo
    echo -e "${YELLOW}Which architecture do you want to build for?${RESET}"
    echo -e " [1] ${GREEN}amd64${RESET}"
    echo -e "  2  ${GREEN}arm64${RESET}"
    echo -e "  3  ${GREEN}arm${RESET}"
    read -p "Pick 1-3 or press [Enter]: " arch_choice

    if [ -z "$arch_choice" ]; then
        arch_choice="1"
    fi

    case $arch_choice in
        1) ARCH_FLAG="amd64" ;;
        2) ARCH_FLAG="arm64" ;;
        3) ARCH_FLAG="arm" ;;
        *)
            echo -e "${RED}Invalid architecture choice.${RESET}"
            exit 1
            ;;
    esac
fi

# If no architecture was set by flags or interactively (should not happen), default:
if [ -z "$ARCH_FLAG" ]; then
    ARCH_FLAG="amd64"
fi

# Build the chosen combo
build_combo "$CHOSEN_OS" "$ARCH_FLAG"

# If we were interactive at any point, show how to repeat
if [ "$INTERACTIVE" = true ] || [ "$INTERACTIVE_ARCH" = true ]; then
    echo
    echo -e "${CYAN}To repeat the command, execute:${RESET}"

    FLAG_STRING=""
    
    # OS Flag
    if [ "$CHOSEN_OS" = "linux" ]; then
        FLAG_STRING="$FLAG_STRING -l"
    elif [ "$CHOSEN_OS" = "windows" ]; then
        FLAG_STRING="$FLAG_STRING -w"
    elif [ "$CHOSEN_OS" = "darwin" ]; then
        FLAG_STRING="$FLAG_STRING -m"
    fi

    # Architecture Flag if not amd64
    if [ "$ARCH_FLAG" != "amd64" ]; then
        FLAG_STRING="$FLAG_STRING --arch=$ARCH_FLAG"
    fi

    if [ -z "$FLAG_STRING" ]; then
        # means windows/amd64 was chosen by default
        FLAG_STRING=" -w"
    fi

    echo -e "  ${BOLD}$0$FLAG_STRING${RESET}"
    echo
fi
