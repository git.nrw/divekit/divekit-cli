package origin

import (
	"divekit/internal"
	"divekit/internal/utils/ars"
	"divekit/internal/utils/resource"
	"fmt"
	"path/filepath"

	"github.com/apex/log"
)

// global variable for the origin repository
var (
	OriginRepo *OriginRepoType
)

// all the relevant paths in the origin repository (all as full paths)
type OriginRepoType struct {
	RepoDir         string
	DistributionMap map[string]*Distribution
	ARSConfig       struct {
		Dir string
	}
}

type Distribution struct {
	Dir                             string
	RepositoryConfigFile            *ars.RepositoryConfigFileType
	IndividualizationConfigFileName string
}

type InitOptions string

const (
	InitIndividualRepositories InitOptions = "InitIndividualRepositories"
	InitRepositoryConfig       InitOptions = "InitRepositoryConfig"
)

func NewOriginRepo(originRepoName string, options ...InitOptions) (*OriginRepoType, error) {
	log.Debug("origin.InitLegacyOriginRepoPaths()")

	originRepo := &OriginRepoType{}
	originRepo.RepoDir = filepath.Join(internal.DivekitHomeDir, originRepoName)
	if err := resource.ValidateAllDirPaths(originRepo.RepoDir); err != nil {
		return nil, err
	}

	if err := originRepo.initDistributions(options...); err != nil {
		return nil, err
	}

	originRepo.ARSConfig.Dir = filepath.Join(originRepo.RepoDir, "ars-config_norepo")

	if err := resource.ValidateAllDirPaths(originRepo.ARSConfig.Dir); err != nil {
		return nil, err
	}

	return originRepo, nil
}

func InitLegacyOriginRepo(originRepoNameFlag string, options ...InitOptions) error {
	if originRepoNameFlag == "" {
		return &OriginRepoError{"The origin repo name flag is not defined"}
	}

	var err error
	OriginRepo, err = NewOriginRepo(originRepoNameFlag, options...)
	if err != nil {
		return err
	}

	return nil
}

func (originRepo *OriginRepoType) GetDistribution(distributionName string) (*Distribution, error) {
	log.Debug("origin.GetDistribution()")
	if distribution := originRepo.DistributionMap[distributionName]; distribution != nil {
		return distribution, nil
	}

	return nil, &OriginRepoError{fmt.Sprintf("The distribution '%s' does not exist", distributionName)}
}

func (originRepo *OriginRepoType) initDistributions(options ...InitOptions) error {
	log.Debug("origin.initDistributions()")
	distributionRootDir := filepath.Join(originRepo.RepoDir, ".divekit_norepo/distributions")

	if originRepo.DistributionMap != nil {
		log.Debug("DistributionMap already initialized, skipping re-initialization")
		return nil
	}

	originRepo.DistributionMap = make(map[string]*Distribution)
	distributionFolders, err := resource.ListSubFolderNames(distributionRootDir)
	if err != nil {
		return fmt.Errorf("The path to the distribution root dir is invalid: %w", err)
	}

	optionSet := make(map[InitOptions]struct{})
	for _, option := range options {
		optionSet[option] = struct{}{}
	}

	for _, distributionName := range distributionFolders {
		distributionFolder := filepath.Join(distributionRootDir, distributionName)
		newDistribution := &Distribution{
			Dir: distributionFolder,
		}
		originRepo.DistributionMap[distributionName] = newDistribution

		if _, ok := optionSet[InitIndividualRepositories]; ok {
			if err := originRepo.initIndividualRepositoriesFile(distributionName, distributionFolder); err != nil {
				return err
			}
		}

		if _, ok := optionSet[InitRepositoryConfig]; ok {
			if err := originRepo.initRepositoryConfigFile(distributionName, distributionFolder); err != nil {
				return err
			}
		}

	}

	return nil
}

func (originRepo *OriginRepoType) initIndividualRepositoriesFile(distributionName string, distributionFolder string) error {
	log.Debug("origin.initIndividualRepositoriesFile()")
	individualRepositoriesFilePath, err := resource.FindUniqueFileWithPrefix(distributionFolder, "individual_repositories")
	if err != nil {
		return fmt.Errorf("The path to the individual_repositories file is invalid: %w", err)
	}

	distribution, ok := originRepo.DistributionMap[distributionName]
	if !ok {
		distribution = &Distribution{}
		originRepo.DistributionMap[distributionName] = distribution
	}
	distribution.IndividualizationConfigFileName = individualRepositoriesFilePath
	return nil
}

func (originRepo *OriginRepoType) initRepositoryConfigFile(distributionName string, distributionFolder string) error {
	log.Debug("origin.initRepositoryConfigFile()")
	repositoryConfigFile, err := ars.NewRepositoryConfigFile(filepath.Join(distributionFolder, "repositoryConfig.json"))
	if err != nil {
		return err
	}

	distribution, ok := originRepo.DistributionMap[distributionName]
	if !ok {
		distribution = &Distribution{}
		originRepo.DistributionMap[distributionName] = distribution
	}
	distribution.RepositoryConfigFile = repositoryConfigFile

	return nil
}

type OriginRepoError struct {
	Msg string
}

func (e *OriginRepoError) Error() string {
	return e.Msg
}
