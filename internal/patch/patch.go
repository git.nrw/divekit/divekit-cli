package patch

import (
	"divekit/internal/utils/resource"
	"fmt"
	"path/filepath"
	"strings"

	"github.com/apex/log"
	"github.com/manifoldco/promptui"
	config "gitlab.com/git.nrw/divekit/modules/config-management"
	gitlabadapter "gitlab.com/git.nrw/divekit/modules/gitlab-adapter"
)

// Global flag variables
var (
	DistributionFlag string
	TokenFlag        string
	BaseURLFlag      string
)

// InitFlags initializes the flags used by the patch command
func InitFlags() {
	// This function is called to initialize any flags needed by the patch package
}

// ShowAndConfirmPlan displays the planned file changes and asks for confirmation
func ShowAndConfirmPlan(
	distribution *config.Distribution,
	baseURL string,
	remoteFiles map[string]*gitlabadapter.FileStatus,
	localFiles map[string]*resource.FileStatus,
) error {
	log.Info("\nPlan:")
	log.Infof("  • Target GitLab instance: %s", baseURL)
	log.Infof("  • Distribution: %s", distribution.Name)
	log.Infof("  • Group: #%d", *distribution.Config.Remotes.GroupIDs["code"])

	// Debug output
	log.Debug("Remote files status:")
	for file, status := range remoteFiles {
		log.Debugf("  %s: exists=%v", file, status.Exists)
	}
	log.Debug("Local files status:")
	for file, status := range localFiles {
		log.Debugf("  %s: exists=%v", file, status.Exists)
	}

	// Collect files by their status
	var toCreate []string // A - Added
	var toUpdate []string // M - Modified
	var toDelete []string // D - Deleted

	// Create a normalized map for local files for easier lookup
	normalizedLocalFiles := make(map[string]*resource.FileStatus)
	for file, status := range localFiles {
		normalizedPath := filepath.ToSlash(file)
		normalizedPath = strings.TrimPrefix(normalizedPath, "./")
		normalizedLocalFiles[normalizedPath] = status
	}

	for file := range remoteFiles {
		existsRemote := remoteFiles[file].Exists
		existsLocal := normalizedLocalFiles[file].Exists

		if existsRemote && existsLocal {
			toUpdate = append(toUpdate, file)
		} else if !existsRemote && existsLocal {
			toCreate = append(toCreate, file)
		} else if existsRemote && !existsLocal {
			toDelete = append(toDelete, file)
		}
	}

	log.Info("\nChanges to be applied:")

	// Sort and display files by status
	if len(toCreate) > 0 {
		for _, file := range toCreate {
			log.Infof("  \x1b[32mA\x1b[0m %s", file) // Green 'A' for added
		}
	}

	if len(toUpdate) > 0 {
		for _, file := range toUpdate {
			log.Infof("  \x1b[33mM\x1b[0m %s", file) // Yellow 'M' for modified
		}
	}

	if len(toDelete) > 0 {
		for _, file := range toDelete {
			log.Infof("  \x1b[31mD\x1b[0m %s", file) // Red 'D' for deleted
		}
	}

	totalChanges := len(toCreate) + len(toUpdate) + len(toDelete)
	log.Infof("\nTotal changes: %d files", totalChanges)

	log.Info("")
	prompt := promptui.Prompt{
		Label:     "Continue with these changes",
		IsConfirm: true,
		Default:   "y",
	}

	result, err := prompt.Run()
	if err != nil || strings.ToLower(result) != "y" {
		log.Info("Operation cancelled by user")
		return fmt.Errorf("operation cancelled")
	}
	return nil
}
