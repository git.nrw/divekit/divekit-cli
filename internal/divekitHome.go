package internal

import (
	"divekit/internal/utils/resource"
	"fmt"
	"os"
	"path/filepath"

	"github.com/apex/log"
)

// Global vars
var (
	DivekitHomeDir string
)

// InitDivekitHomeDir initializes the home directory of all the Divekit repos. It is set by the
// --home flag
// the DIVEKIT_HOME environment variable
// the existence of a .divekit/modules directory
// or the current working directory (in this order).
func InitDivekitHomeDir(divekitHomeFlag string) error {
	log.Debug("config.InitDivekitHomeDir()")
	setDivekitHomeDirFromVariousSources(divekitHomeFlag)
	if err := resource.ValidateAllDirPaths(DivekitHomeDir); err != nil {
		return err
	}

	log.WithFields(log.Fields{
		"DivekitHomeDir": DivekitHomeDir,
	}).Info("Setting Divekit Home Dir")

	return nil
}

func setDivekitHomeDirFromVariousSources(divekitHomeFlag string) {
	if checkFlag(divekitHomeFlag) {
		return
	} else if checkEnvDivekitHome() {
		return
	} else if checkUserHomeDir() {
		return
	} else {
		checkWorkingDir()
	}
}

func checkFlag(divekitHomeFlag string) bool {
	if divekitHomeFlag != "" {
		log.Info("Home dir is set via flag -m / --home: " + divekitHomeFlag)
		DivekitHomeDir = divekitHomeFlag
		return true
	}
	return false
}

func checkEnvDivekitHome() bool {
	envHome := os.Getenv("DIVEKIT_HOME")
	if envHome != "" {
		log.Info("Home dir is set via DIVEKIT_HOME environment variable: " + envHome)
		DivekitHomeDir = envHome
		return true
	}
	return false
}

func checkUserHomeDir() bool {
	homeDir, err := os.UserHomeDir()
	if err == nil {
		modulesPath := filepath.Join(homeDir, ".divekit")
		if resource.FileExists(modulesPath) {
			log.Info("Home dir is set via existing .divekit directory in: " + homeDir)
			DivekitHomeDir = homeDir
			return true
		}
	}
	return false
}

func checkWorkingDir() {
	workingDir, _ := os.Getwd()
	log.Info("Home dir set to current directory: " + workingDir)
	DivekitHomeDir = workingDir
}

// GetDivekitHome returns the current Divekit home directory
func GetDivekitHome() (string, error) {
	homeDir, err := os.UserHomeDir()
	if err != nil {
		return "", fmt.Errorf("failed to get user home directory: %v", err)
	}

	divekitHome := filepath.Join(homeDir, ".divekit")

	// Create the directory if it doesn't exist
	if err := os.MkdirAll(divekitHome, 0755); err != nil {
		return "", fmt.Errorf("failed to create divekit home directory: %v", err)
	}

	return divekitHome, nil
}
