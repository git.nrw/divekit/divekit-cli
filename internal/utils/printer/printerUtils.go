package printer

import (
	"divekit/internal/utils/dye"
	"fmt"
	"strings"
)

const (
	SUCCESS = "SUCCESS"
	WARNING = "WARNING"
	ERROR   = "ERROR"
	INFO    = "INFO"
)

var (
	SuccessIcon         = dye.Green("[√]")
	SuccessIconIndented = dye.Green("•")
	WarningIcon         = dye.Yellow("[!]")
	WarningIconIndented = dye.Yellow("!")
	ErrorIcon           = dye.Red("[X]")
	ErrorIconIndented   = dye.Red("X")
	InfoIcon            = dye.Blue("[i]")
	InfoIconIndented    = dye.Blue("i")
)

type StatusPrinter struct {
	mainStatus string
	messages   []string
}

func NewStatusPrinter(mainStatus string) *StatusPrinter {
	return &StatusPrinter{
		mainStatus: mainStatus,
	}
}

func (sp *StatusPrinter) AddSubMessage(status, message string, level int) {
	indentation := strings.Repeat("    ", level)
	var icon string

	switch status {
	case SUCCESS:
		if level == 0 {
			icon = SuccessIcon
		} else {
			icon = SuccessIconIndented
		}
	case WARNING:
		if level == 0 {
			icon = WarningIcon
		} else {
			icon = WarningIconIndented
		}
	case ERROR:
		if level == 0 {
			icon = ErrorIcon
		} else {
			icon = ErrorIconIndented
		}
	case INFO:
		if level == 0 {
			icon = InfoIcon
		} else {
			icon = InfoIconIndented
		}
	default:
		icon = message
	}

	sp.messages = append(sp.messages, fmt.Sprintf("%s%s %s", indentation, icon, message))
}

func (sp *StatusPrinter) SetMainStatus(status string) {
	sp.mainStatus = status
}

func (sp *StatusPrinter) PrintMainStatus() {
	switch sp.mainStatus {
	case SUCCESS:
		fmt.Printf("%s %s\n", SuccessIcon, "Modules installed")
	case ERROR:
		fmt.Printf("%s %s\n", ErrorIcon, "Modules installation failed")
	default:
		fmt.Printf("%s\n", "Modules status unknown")
	}
	for _, msg := range sp.messages {
		fmt.Println(msg)
	}
	sp.messages = []string{}
}

func PrintStatus(status, message string, level int) {
	indentation := strings.Repeat("    ", level)
	var icon string

	switch status {
	case SUCCESS:
		if level == 0 {
			icon = SuccessIcon
		} else {
			icon = SuccessIconIndented
		}
	case WARNING:
		if level == 0 {
			icon = WarningIcon
		} else {
			icon = WarningIconIndented
		}
	case ERROR:
		if level == 0 {
			icon = ErrorIcon
		} else {
			icon = ErrorIconIndented
		}
	case INFO:
		if level == 0 {
			icon = InfoIcon
		} else {
			icon = InfoIconIndented
		}
	default:
		icon = message
	}

	fmt.Printf("%s%s %s\n", indentation, icon, message)
}

type TempStatusMessage struct {
	message string
	level   int
}

func PrintTempStatus(status, message string, level int) *TempStatusMessage {
	indentation := strings.Repeat("    ", level)
	var icon string

	switch status {
	case SUCCESS:
		icon = SuccessIcon
	case WARNING:
		icon = WarningIcon
	case ERROR:
		icon = ErrorIcon
	case INFO:
		icon = InfoIcon
	default:
		icon = message
	}

	fmt.Printf("%s%s %s\n", indentation, icon, message)
	return &TempStatusMessage{message: message, level: level}
}

func (tsm *TempStatusMessage) Resolve() {
	RemoveLastLine()
}

func RemoveLastLine() {
	cursorUp()
	cursorEndOfLine()
}

func cursorUp() {
	fmt.Print("\033[1A")
}

func cursorEndOfLine() {
	fmt.Print("\033[K")
}
