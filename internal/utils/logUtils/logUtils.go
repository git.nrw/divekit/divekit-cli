package logUtils

import (
	"fmt"
	"io"
	"os"
	"sync"

	"divekit/internal/utils/dye"

	"github.com/apex/log"
	"golang.org/x/text/cases"
	"golang.org/x/text/language"
)

type Chattyness int

const (
	Silent Chattyness = iota
	Verbose
	VeryVerbose
)

var (
	LogLevel = log.InfoLevel
)

type CustomHandler struct {
	mu sync.Mutex
	w  io.Writer
}

func (h *CustomHandler) HandleLog(e *log.Entry) error {
	h.mu.Lock()
	defer h.mu.Unlock()

	var msg string
	if e.Level == log.InfoLevel {
		msg = fmt.Sprintf("%s", e.Message)
	} else {
		levelStr := cases.Title(language.English).String(e.Level.String())
		switch e.Level {
		case log.WarnLevel:
			msg = fmt.Sprintf("[%s] %s", dye.Yellow(levelStr), e.Message)
		case log.ErrorLevel:
			msg = fmt.Sprintf("[%s] %s", dye.Red(levelStr), e.Message)
		case log.DebugLevel:
			msg = fmt.Sprintf("[%s] %s", dye.Grey(levelStr), e.Message)
		default:
			msg = fmt.Sprintf("[%s] %s", levelStr, e.Message)
		}
	}

	msg += "\n"

	if LogLevel == log.DebugLevel && len(e.Fields) > 0 {
		for k, v := range e.Fields {
			msg += fmt.Sprintf("  %s: %v\n", k, v)
		}
	}

	_, err := h.w.Write([]byte(msg))
	return err
}

func NewCustomHandler(w io.Writer) *CustomHandler {
	return &CustomHandler{
		w: w,
	}
}

func DefineLoggingLevel(logLevelString string) error {
	var err error = nil

	customHandler := NewCustomHandler(os.Stdout)
	log.SetHandler(customHandler)

	LogLevel, err = StringAsLogLevel(logLevelString)
	log.SetLevel(LogLevel)
	log.Info("Log level set to " + LogLevelAsString() + ".")
	return err
}

func LogLevelAsString() string {
	switch LogLevel {
	case log.DebugLevel:
		return "debug"
	case log.InfoLevel:
		return "info"
	case log.WarnLevel:
		return "warning"
	case log.ErrorLevel:
		return "error"
	default:
		return "info"
	}
}

// LogLevelError represents an error for invalid log levels
type LogLevelError struct {
	msg string
}

func (e *LogLevelError) Error() string {
	return e.msg
}

func StringAsLogLevel(levelStr string) (log.Level, error) {
	switch levelStr {
	case "debug":
		return log.DebugLevel, nil
	case "info":
		return log.InfoLevel, nil
	case "warning":
		return log.WarnLevel, nil
	case "error":
		return log.ErrorLevel, nil
	default:
		return log.InfoLevel, &LogLevelError{
			msg: fmt.Sprintf("invalid log level: '%s'. Use one of: debug, info, warning, error", levelStr),
		}
	}
}
