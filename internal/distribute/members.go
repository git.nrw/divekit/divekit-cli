package distribute

import (
	"divekit/internal/utils/dye"

	"github.com/apex/log"
)

func LogMemberStatus(available, unavailable []string) {
	log.Info("Member status:")
	if len(available) > 0 {
		log.Infof("[%s] %d users available", dye.Green("√"), len(available))
	}
	if len(unavailable) > 0 {
		log.Infof("[%s] %d users not found:", dye.Red("X"), len(unavailable))
		for _, member := range unavailable {
			log.Infof("    - " + member)
		}
	}
}
