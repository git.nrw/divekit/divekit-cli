package distribute

import (
	"fmt"
	"strings"

	"github.com/apex/log"
	"github.com/manifoldco/promptui"
	"github.com/schollz/progressbar/v3"
	"github.com/xanzy/go-gitlab"
	config "gitlab.com/git.nrw/divekit/modules/config-management"
	gitlabadapter "gitlab.com/git.nrw/divekit/modules/gitlab-adapter"
)

func ShowAndConfirmPlan(distribution *config.Distribution, baseURL string, memberStatus *config.MemberStatus) error {

	LogMemberStatus(memberStatus.Available, memberStatus.Unavailable)

	repoName := config.GetFirstNonEmpty(
		distribution.Config.Remotes.Name,
		config.DefaultConfig.Remotes.Name,
	)

	log.Info("\nPlan:")
	log.Infof("  • Create %d projects named \"%s\"", len(memberStatus.Available), repoName)
	log.Infof("  • On GitLab instance: %s", baseURL)
	log.Infof("  • In group: #%d", *distribution.Config.Remotes.GroupIDs["code"])
	log.Infof("  • Assign %d members (%d per repository)", len(memberStatus.Available), 1)
	log.Info("")

	prompt := promptui.Prompt{
		Label:     "Continue",
		IsConfirm: true,
		Default:   "y",
	}

	result, err := prompt.Run()
	if err != nil || strings.ToLower(result) != "y" {
		log.Info("Operation cancelled by user")
		return fmt.Errorf("operation cancelled")
	}
	return nil
}

func CreateAndSetupProjects(workingDir string, distribution *config.Distribution, token, baseURL string, members *config.MemberStatus) (interface{}, error) {
	codeGroupID := distribution.Config.Remotes.GroupIDs["code"]
	if codeGroupID == nil || *codeGroupID <= 0 {
		return nil, NewDistributeError(ErrInvalidGroupID, fmt.Sprintf("invalid code group ID: %v", codeGroupID), nil)
	}

	gitLabClient, err := gitlabadapter.NewGitLabClient(token, baseURL)
	if err != nil {
		return nil, NewDistributeError(ErrGitLabConnection, "failed to create GitLab client", err)
	}

	log.Infof("\nCreating projects at #%d...", *codeGroupID)
	bar := progressbar.Default(int64(len(members.Available)))

	groupsMap := make(map[string][]string)
	if distribution.Config.MembersPath != "" {

		membersConfig, err := config.LoadMembers(distribution, distribution.Config.MembersPath)
		if err != nil {
			return nil, fmt.Errorf("failed to load members: %v", err)
		}
		groupsMap = gitlabadapter.ConvertMembersToMap(membersConfig)
	}

	projects, mappings, err := gitLabClient.CreateDistributionProjects(
		groupsMap,
		distribution.Config.Remotes.Name,
		*codeGroupID,
		members.Available,
	)
	if err != nil {
		return nil, fmt.Errorf("failed to create projects: %v", err)
	}
	bar.Finish()

	if err := uploadAndAssignProjects(workingDir, gitLabClient, projects, members.Available); err != nil {
		return nil, NewDistributeError(ErrFileUpload, "failed to upload and assign projects", err)
	}

	return mappings, nil
}

func uploadAndAssignProjects(workingDir string, client *gitlabadapter.GitLabType, projects []*gitlab.Project, availableMembers []string) error {
	commitInfo := gitlabadapter.CommitInfo{
		AuthorName:  "Divekit",
		AuthorEmail: "divekit@git.nrw",
		Message:     "Initial commit",
	}

	log.Info("\nUploading files...")
	bar := progressbar.Default(int64(len(projects)))
	if err := client.UploadLocalProject(workingDir, projects, commitInfo); err != nil {
		log.Error("Upload failed")
		return fmt.Errorf("failed to upload project to projects: %v", err)
	}
	bar.Finish()

	log.Info("\nAssigning members...")
	bar = progressbar.Default(int64(len(projects)))
	if err := client.AssignMembersToProjects(projects, availableMembers); err != nil {
		return fmt.Errorf("failed to assign members: %v", err)
	}
	bar.Finish()

	return nil
}
