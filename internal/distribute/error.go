package distribute

import (
	"fmt"
	"strings"
)

// DistributeError represents a specific error in the distribute process
type DistributeError struct {
	ErrorType ErrorType
	Message   string
	Err       error
}

// ErrorType defines the different types of errors
type ErrorType int

const (
	// Critical errors that lead to termination
	ErrConfigLoad ErrorType = iota
	ErrWorkingDir
	ErrGitLabConnection
	ErrProjectCreation
	ErrFileUpload
	ErrInvalidGroupID

	// Non-critical errors that trigger a warning
	ErrMembersNotFound
)

func (e *DistributeError) Error() string {
	if e.Err != nil {
		return fmt.Sprintf("%s: %v", e.Message, e.Err)
	}
	return e.Message
}

// IsCritical determines whether the error is critical and should lead to termination
func (e *DistributeError) IsCritical() bool {
	switch e.ErrorType {
	case ErrMembersNotFound:
		return false
	default:
		return true
	}
}

// NewDistributeError creates a new DistributeError
func NewDistributeError(errorType ErrorType, message string, err error) *DistributeError {
	return &DistributeError{
		ErrorType: errorType,
		Message:   message,
		Err:       err,
	}
}

// NewMembersNotFoundError creates a specific error for members not found
func NewMembersNotFoundError(unavailableMembers []string) *DistributeError {
	message := fmt.Sprintf("Following members were not found: %s", strings.Join(unavailableMembers, ", "))
	return &DistributeError{
		ErrorType: ErrMembersNotFound,
		Message:   message,
	}
}
