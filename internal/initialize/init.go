package initialize

import (
	"bufio"
	"divekit/internal"
	"encoding/csv"
	"fmt"
	"os"
	"path/filepath"
	"strconv"
	"strings"

	config "gitlab.com/git.nrw/divekit/modules/config-management"

	"github.com/apex/log"
	"github.com/charmbracelet/bubbles/textinput"
	tea "github.com/charmbracelet/bubbletea"
	"github.com/charmbracelet/lipgloss"
	"github.com/google/uuid"
)

type initModel struct {
	Distribution textinput.Model
	RepoName     textinput.Model
	CodeGroupID  textinput.Model
	TestGroupID  textinput.Model
	currentInput int
	done         bool
	err          error
	config       *config.MinimalConfig
	ResolvedPath string
}

func initialModel(existingConfig config.MinimalConfig) initModel {
	distribution := textinput.New()
	distribution.Placeholder = "sandbox"
	distribution.Focus()

	repoName := textinput.New()
	repoName.Placeholder = "{{uuid}}"

	codeGroupID := textinput.New()
	if existingConfig.Remotes.GroupIDs["code"] != nil {
		codeGroupID.Placeholder = fmt.Sprintf("%d", *existingConfig.Remotes.GroupIDs["code"])
	}

	testGroupID := textinput.New()
	if existingConfig.Remotes.GroupIDs["test"] != nil {
		testGroupID.Placeholder = fmt.Sprintf("%d", *existingConfig.Remotes.GroupIDs["test"])
	}

	return initModel{
		Distribution: distribution,
		RepoName:     repoName,
		CodeGroupID:  codeGroupID,
		TestGroupID:  testGroupID,
		config:       &existingConfig,
	}
}

func (m initModel) Update(msg tea.Msg) (tea.Model, tea.Cmd) {
	var cmd tea.Cmd

	switch msg := msg.(type) {
	case tea.KeyMsg:
		switch msg.String() {
		case "ctrl+c":
			return m, tea.Quit
		case "enter":
			// Validate and update repo name if necessary
			if m.currentInput == 1 && !strings.Contains(m.RepoName.Value(), "{{uuid}}") {
				m.RepoName.SetValue(m.RepoName.Value() + "-{{uuid}}")
			}

			m.currentInput++
			if m.currentInput > 3 {
				m.done = true
				return m, tea.Quit
			}

			// Set focus to the next input field
			switch m.currentInput {
			case 1:
				m.RepoName.Focus()
				m.Distribution.Blur()
			case 2:
				m.CodeGroupID.Focus()
				m.RepoName.Blur()
			case 3:
				m.TestGroupID.Focus()
				m.CodeGroupID.Blur()
			}

			return m, nil
		}
	}

	// Handle input updates
	switch m.currentInput {
	case 0:
		m.Distribution, cmd = m.Distribution.Update(msg)
	case 1:
		m.RepoName, cmd = m.RepoName.Update(msg)
	case 2:
		m.CodeGroupID, cmd = m.CodeGroupID.Update(msg)
	case 3:
		m.TestGroupID, cmd = m.TestGroupID.Update(msg)
	}

	return m, cmd
}

func (m initModel) View() string {
	var b strings.Builder
	b.WriteString("\nDivekit Repository Configuration\n\n")

	inputs := []struct {
		label string
		input textinput.Model
	}{
		{"Distribution", m.Distribution},
		{"Repository Name", m.RepoName},
		{"Code Group ID", m.CodeGroupID},
		{"Test Group ID", m.TestGroupID},
	}

	for i, input := range inputs {
		if i == m.currentInput {
			b.WriteString(lipgloss.NewStyle().Foreground(lipgloss.Color("42")).Render(">"))
		} else {
			b.WriteString(" ")
		}
		b.WriteString(" " + input.label + ": " + input.input.View() + "\n")
	}

	return b.String()
}

func promptRepoName(reader *bufio.Reader, prompt, defaultValue string) string {
	input := promptString(reader, prompt, defaultValue)
	if !strings.Contains(input, "{{uuid}}") {
		input += "-{{uuid}}"
	}
	return input
}

func promptString(reader *bufio.Reader, prompt, defaultValue string) string {
	fmt.Printf("? %s", prompt)
	if defaultValue != "" {
		fmt.Printf(" [%s]", defaultValue)
	}
	fmt.Print(": ")
	input, _ := reader.ReadString('\n')
	input = strings.TrimSpace(input)
	if input == "" {
		return defaultValue
	}
	return input
}

func promptIntPtr(reader *bufio.Reader, prompt string, defaultValue *int) *int {
	for {
		var defaultValueStr string
		if defaultValue != nil {
			defaultValueStr = fmt.Sprintf("%d", *defaultValue)
		}
		input := promptString(reader, prompt, defaultValueStr)
		input = strings.TrimSpace(strings.ToLower(input))

		if input == "" || input == "null" || input == "0" {
			return nil
		}

		value, err := strconv.Atoi(input)
		if err == nil {
			return &value
		}
		log.Warn("Invalid input. Please enter a number, or 'null' / '0' for no value.")
	}
}

func getExistingOrDefault(existing string, defaultValue string) string {
	if existing != "" {
		return existing
	}
	return defaultValue
}

type memberActionModel struct {
	cursor  int
	options []string
	chosen  string
}

func newMemberActionModel() memberActionModel {
	return memberActionModel{
		options: []string{"merge with new", "replace with new", "keep", "change path"},
	}
}

func (m memberActionModel) Init() tea.Cmd {
	return nil
}

func (m memberActionModel) Update(msg tea.Msg) (tea.Model, tea.Cmd) {
	switch msg := msg.(type) {
	case tea.KeyMsg:
		switch msg.String() {
		case "ctrl+c", "q":
			return m, tea.Quit
		case "up", "k":
			if m.cursor > 0 {
				m.cursor--
			}
		case "down", "j":
			if m.cursor < len(m.options)-1 {
				m.cursor++
			}
		case "enter":
			m.chosen = m.options[m.cursor]
			return m, tea.Quit
		}
	}
	return m, nil
}

func (m memberActionModel) View() string {
	s := "\nSelect members action:\n\n"
	for i, option := range m.options {
		cursor := " "
		if m.cursor == i {
			cursor = lipgloss.NewStyle().Foreground(lipgloss.Color("42")).Render(">")
		}
		s += fmt.Sprintf("%s %s\n", cursor, option)
	}
	return s
}

func handleMembersAction(minimalConfig *config.MinimalConfig) (string, error) {
	p := tea.NewProgram(newMemberActionModel())
	m, err := p.Run()
	if err != nil {
		return "", fmt.Errorf("Error running member action selector: %v", err)
	}

	selectedOption := m.(memberActionModel).chosen
	switch selectedOption {
	case "keep":
		log.Debug("Keeping existing members configuration")
		return "", nil
	case "merge with new", "replace with new":
		p := tea.NewProgram(newFilePickerModel(selectedOption, minimalConfig))
		model, err := p.Run()
		if err != nil {
			return "", fmt.Errorf("Error running file input: %v", err)
		}

		finalModel := model.(filePickerModel)
		if finalModel.err != nil {
			return "", finalModel.err
		}
		return finalModel.resolvedPath, nil
	case "change path":
		// Here also use a textinput model
		p := tea.NewProgram(newPathInputModel(minimalConfig))
		model, err := p.Run()
		if err != nil {
			return "", fmt.Errorf("Error running path input: %v", err)
		}

		finalModel := model.(pathInputModel)
		if finalModel.err != nil {
			return "", finalModel.err
		}
	}

	return "", nil
}

// New model for the path input
type pathInputModel struct {
	textInput textinput.Model
	config    *config.MinimalConfig
	err       error
}

func newPathInputModel(config *config.MinimalConfig) pathInputModel {
	ti := textinput.New()
	ti.Placeholder = "Enter new path for members file"
	ti.Focus()
	ti.Width = 40
	ti.CharLimit = 512

	return pathInputModel{
		textInput: ti,
		config:    config,
	}
}

func (m pathInputModel) Init() tea.Cmd {
	return textinput.Blink
}

func (m pathInputModel) Update(msg tea.Msg) (tea.Model, tea.Cmd) {
	var cmd tea.Cmd

	switch msg := msg.(type) {
	case tea.KeyMsg:
		switch msg.String() {
		case "ctrl+c", "esc":
			return m, tea.Quit
		case "enter":
			if path := m.textInput.Value(); path != "" {
				m.config.MembersPath = path
				return m, tea.Quit
			}
		}
	}

	m.textInput, cmd = m.textInput.Update(msg)
	return m, cmd
}

func (m pathInputModel) View() string {
	return "\nEnter new path for members file:\n" + m.textInput.View() + "\n"
}

func containsUsername(members *config.MembersConfig, username string) bool {
	for _, group := range members.Groups {
		for _, member := range group.Members {
			if member == username {
				return true
			}
		}
	}
	return false
}

func createNewMembersFilePath() (string, error) {
	fileUUID := uuid.New().String()
	fileName := fmt.Sprintf("divekit-members-%s.json", fileUUID)

	// Return path with environment variable prefix
	return filepath.Join(config.MembersDirPrefix, fileName), nil
}

func importMembersFromCSV(csvPath string) (*config.MembersConfig, error) {
	file, err := os.Open(csvPath)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	reader := csv.NewReader(file)
	lines, err := reader.ReadAll()
	if err != nil {
		return nil, err
	}

	var groups []config.Group
	for _, line := range lines[1:] {
		if len(line) > 0 {
			groups = append(groups, config.Group{
				UUID:    uuid.New().String(),
				Members: []string{line[0]},
			})
		}
	}

	return &config.MembersConfig{
		Version: "2.0",
		Groups:  groups,
	}, nil
}

// Init function for the tea.Model interface
func (m initModel) Init() tea.Cmd {
	return textinput.Blink
}

// New model for the file selection
type filePickerModel struct {
	textInput    textinput.Model
	action       string
	config       *config.MinimalConfig
	err          error
	resolvedPath string
}

func newFilePickerModel(action string, config *config.MinimalConfig) filePickerModel {
	ti := textinput.New()
	ti.Placeholder = "../members/latest.csv"
	ti.Focus()
	ti.Width = 40
	ti.CharLimit = 512

	return filePickerModel{
		textInput: ti,
		action:    action,
		config:    config,
	}
}

func (m filePickerModel) View() string {
	return "\nEnter path to CSV file:\n" + m.textInput.View() + "\n"
}

func (m filePickerModel) Init() tea.Cmd {
	return textinput.Blink
}

func (m filePickerModel) Update(msg tea.Msg) (tea.Model, tea.Cmd) {
	var cmd tea.Cmd

	switch msg := msg.(type) {
	case tea.KeyMsg:
		switch msg.String() {
		case "ctrl+c", "esc":
			return m, tea.Quit
		case "enter":
			if path := m.textInput.Value(); path != "" {
				if _, err := os.Stat(path); err != nil {
					m.err = fmt.Errorf("File not found: %v", err)
					return m, tea.Quit
				}

				newMembers, err := importMembersFromCSV(path)
				if err != nil {
					m.err = fmt.Errorf("Error importing members: %v", err)
					return m, tea.Quit
				}

				divekitHome, err := internal.GetDivekitHome()
				if err != nil {
					m.err = fmt.Errorf("Error getting divekit home: %v", err)
					return m, tea.Quit
				}

				if m.action == "merge with new" && m.config.MembersPath != "" {
					existingMembers, err := config.LoadMembers(&config.Distribution{
						Config: m.config,
					}, divekitHome)
					if err != nil {
						m.err = fmt.Errorf("Error loading existing members: %v", err)
						return m, tea.Quit
					}

					// Merge members
					for _, newGroup := range newMembers.Groups {
						for _, member := range newGroup.Members {
							if !containsUsername(existingMembers, member) {
								existingMembers.Groups = append(existingMembers.Groups, newGroup)
							}
						}
					}
					newMembers = existingMembers
				}

				// Create new path for the members file
				if m.config.MembersPath == "" {
					membersFilePath, err := createNewMembersFilePath()
					if err != nil {
						m.err = fmt.Errorf("Error creating new members file path: %v", err)
						return m, tea.Quit
					}
					m.config.MembersPath = membersFilePath
				}

				// Save the members file
				resolvedPath, err := config.ResolveMembersPath(m.config.MembersPath, divekitHome)
				if err != nil {
					m.err = fmt.Errorf("Error resolving members path: %v", err)
					return m, tea.Quit
				}

				m.resolvedPath = resolvedPath

				if err := os.MkdirAll(filepath.Dir(resolvedPath), 0755); err != nil {
					m.err = fmt.Errorf("Error creating directory: %v", err)
					return m, tea.Quit
				}

				if err := newMembers.SaveToFile(resolvedPath, divekitHome); err != nil {
					m.err = fmt.Errorf("Error saving members: %v", err)
					return m, tea.Quit
				}

				return m, tea.Quit
			}
		}
	}

	m.textInput, cmd = m.textInput.Update(msg)
	return m, cmd
}

func DisplayWelcomeMessage() {
	log.Info("This utility will walk you through creating or updating the")
	log.Info("necessary configuration files for your 'Origin' Divekit repository.")
	log.Info("")
	log.Info("It only covers the most common items, and tries to")
	log.Info("guess sensible defaults or use existing values.")
	log.Info("")
	log.Info("Press ^C at any time to quit.")
	log.Info("")
}

func LoadExistingConfiguration() (config.MinimalConfig, error) {
	var existingConfig config.MinimalConfig
	divekitHome, err := internal.GetDivekitHome()
	if err == nil {
		defaultConfigPath := filepath.Join(divekitHome, "config", "default", "origin.json")
		if defaultCfg, err := config.LoadConfigFromFile(defaultConfigPath); err == nil {
			existingConfig = *defaultCfg
		} else {
			existingConfig = config.DefaultConfig
		}
	}
	return existingConfig, nil
}

func RunInteractivePrompt(existingConfig config.MinimalConfig) (initModel, error) {
	p := tea.NewProgram(initialModel(existingConfig))
	model, err := p.Run()
	if err != nil {
		return initModel{}, err
	}
	return model.(initModel), nil
}

func UpdateConfigurationFromModel(existingConfig config.MinimalConfig, model initModel) config.MinimalConfig {
	config := existingConfig
	config.Remotes.Name = model.RepoName.Value()

	// Process GroupIDs
	if model.CodeGroupID.Value() != "" {
		if codeID, err := strconv.Atoi(model.CodeGroupID.Value()); err == nil {
			config.Remotes.GroupIDs["code"] = &codeID
		}
	}

	if model.TestGroupID.Value() != "" {
		if testID, err := strconv.Atoi(model.TestGroupID.Value()); err == nil {
			config.Remotes.GroupIDs["test"] = &testID
		}
	}

	// Set defaults if needed
	if config.Host.URL == "" {
		config.Host = existingConfig.Host
	}
	if config.Version == "" {
		config.Version = "2.0"
	}

	return config
}

func SaveConfiguration(config config.MinimalConfig, distribution string) error {
	if distribution == "" {
		distribution = "sandbox"
	}

	configDir := filepath.Join(".divekit_norepo", "distributions", distribution)
	configPath := filepath.Join(configDir, "config.json")

	if err := os.MkdirAll(configDir, 0755); err != nil {
		log.WithError(err).Error("Failed to create configuration directory")
		return err
	}

	resolvedPath, err := handleMembersAction(&config)
	if err != nil {
		log.WithError(err).Error("Failed to process members")
		return err
	}

	if err := config.SaveToFile(configPath); err != nil {
		log.WithError(err).Error("Failed to save configuration")
		return err
	}

	DisplaySuccessMessage(config, configPath, resolvedPath)
	return nil
}

func DisplaySuccessMessage(config config.MinimalConfig, configPath string, resolvedPath string) {
	log.Info("")
	log.Info("Configuration created successfully!")
	log.Info("")
	log.Infof("Repository configuration created/updated at:\n%s", configPath)

	if config.MembersPath != "" {
		log.Info("")
		if resolvedPath != "" {
			absPath, _ := filepath.Abs(resolvedPath)
			log.Infof("Members configuration saved at:\n%s", absPath)
		} else {
			absPath, _ := filepath.Abs(config.MembersPath)
			log.Infof("Members configuration saved at:\n%s", absPath)
		}
	}
	log.Info("")
}
