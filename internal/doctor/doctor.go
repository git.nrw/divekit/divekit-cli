package doctor

import (
	"divekit/internal"
	"divekit/internal/utils/printer"
	"fmt"
	"io"
	"os"
	"os/exec"
	"path/filepath"

	config "gitlab.com/git.nrw/divekit/modules/config-management"

	"github.com/apex/log"
	"github.com/spf13/viper"
	gitlabadapter "gitlab.com/git.nrw/divekit/modules/gitlab-adapter"
)

func CheckTools() error {
	if !checkTool("git") {
		printer.PrintStatus(printer.ERROR, "git not available", 0)
		return fmt.Errorf("git is not available")
	}
	printer.PrintStatus(printer.SUCCESS, "git available", 0)

	if !checkTool("npm") {
		printer.PrintStatus(printer.ERROR, "npm not available", 0)
		return fmt.Errorf("npm is not available")
	}
	printer.PrintStatus(printer.SUCCESS, "npm available", 0)

	return nil
}

func checkTool(tool string) bool {
	_, err := exec.LookPath(tool)
	return err == nil
}

func CheckGitLabToken() error {
	// Load default config
	divekitHome, err := internal.GetDivekitHome()
	if err != nil {
		printer.PrintStatus(printer.ERROR, fmt.Sprintf("Failed to get divekit home: %v", err), 0)
		return err
	}

	defaultConfigPath := filepath.Join(divekitHome, "config", "default", "origin.json")
	defaultInitConfig, err := config.LoadConfigFromFile(defaultConfigPath)
	if err != nil {
		log.Debug("Could not load default/origin.json, using hardcoded defaults")
		defaultInitConfig = &config.DefaultConfig
	}

	// Get token from environment using viper
	v := viper.New()
	v.SetEnvPrefix("")
	v.AutomaticEnv()

	// Try to load .env file
	envPath := filepath.Join(divekitHome, ".env")
	log.Debug("Attempting to load .env from: " + envPath)

	v.SetConfigFile(envPath)
	v.SetConfigType("env")
	if err := v.ReadInConfig(); err != nil {
		log.Debug("Could not read .env file")
	}

	tokenEnvVar := config.GetFirstNonEmpty(
		defaultInitConfig.Host.TokenEnvVar,
		config.DefaultConfig.Host.TokenEnvVar,
	)

	token := v.GetString(tokenEnvVar)
	if token == "" {
		printer.PrintStatus(printer.ERROR, "GitLab token not set", 0)
		printer.PrintStatus(printer.INFO, "To set up your GitLab token:", 1)
		printer.PrintStatus(printer.INFO, fmt.Sprintf("1. Set environment variable %s", tokenEnvVar), 2)
		printer.PrintStatus(printer.INFO, "2. Or create .env file in Divekit home directory", 2)
		return fmt.Errorf("GitLab token not set")
	}

	// Get base URL from config
	baseURL := config.GetFirstNonEmpty(
		defaultInitConfig.Host.URL,
		config.DefaultConfig.Host.URL,
	)

	client, err := gitlabadapter.NewGitLabClient(token, baseURL)
	if err != nil {
		printer.PrintStatus(printer.ERROR, fmt.Sprintf("Failed to create GitLab client: %v", err), 0)
		return err
	}

	permissions, err := client.CheckTokenPermissions()
	if err != nil {
		printer.PrintStatus(printer.ERROR, fmt.Sprintf("Failed to check token permissions: %v", err), 0)
		return err
	}

	statusPrinter := printer.NewStatusPrinter(printer.INFO)
	statusPrinter.SetMainStatus(printer.INFO)
	statusPrinter.AddSubMessage(printer.INFO, "Checking GitLab token permissions...", 0)

	if !permissions.TokenIsValid {
		statusPrinter.SetMainStatus(printer.ERROR)
		statusPrinter.AddSubMessage(printer.ERROR, "Token is invalid", 1)
		statusPrinter.PrintMainStatus()
		return fmt.Errorf("token is invalid")
	}

	if !permissions.HasValidScopes {
		statusPrinter.SetMainStatus(printer.ERROR)
		statusPrinter.AddSubMessage(printer.ERROR, "Token lacks required 'api' scope", 1)
		statusPrinter.AddSubMessage(printer.INFO, "Please update your token with the required scope", 1)
		statusPrinter.PrintMainStatus()
		return fmt.Errorf("token lacks required 'api' scope")
	}

	statusPrinter.SetMainStatus(printer.SUCCESS)
	statusPrinter.AddSubMessage(printer.SUCCESS, "Token has all required permissions", 1)
	statusPrinter.PrintMainStatus()
	return nil
}

func SetupDivekit() error {
	homeDir, err := os.UserHomeDir()
	if err != nil {
		printer.PrintStatus(printer.ERROR, fmt.Sprintf("Unable to get home directory: %v", err), 0)
		return err
	}

	divekitDir := filepath.Join(homeDir, ".divekit")
	modulesDir := filepath.Join(divekitDir, "modules")
	automatedRepoSetupDir := filepath.Join(modulesDir, "automated-repo-setup")
	repoEditorDir := filepath.Join(modulesDir, "repo-editor")

	if !dirExists(divekitDir) {
		log.Infof("Creating directory: %s", divekitDir)
		err := os.MkdirAll(divekitDir, 0755)
		if err != nil {
			log.Fatalf("Unable to create directory: %v", err)
		}
	}

	if !dirExists(modulesDir) {
		log.Infof("Creating directory: %s", modulesDir)
		err := os.MkdirAll(modulesDir, 0755)
		if err != nil {
			log.Fatalf("Unable to create directory: %v", err)
		}
	}

	modulesInstalled := true
	statusPrinter := printer.NewStatusPrinter(printer.SUCCESS)

	installingModulesMessage := printer.PrintTempStatus(printer.INFO, "Installing modules...", 0)

	if !dirExists(automatedRepoSetupDir) {
		if cloneRepo("https://gitlab.com/divekit-tools/legacy/automated-repo-setup.git", automatedRepoSetupDir) {
			if runNpmInstall(automatedRepoSetupDir) {
				statusPrinter.AddSubMessage(printer.SUCCESS, "'Automated Repo Setup' was installed", 1)
			} else {
				modulesInstalled = false
				statusPrinter.AddSubMessage(printer.ERROR, "Initializing 'Automated Repo Setup' failed", 1)
			}
		} else {
			modulesInstalled = false
			statusPrinter.AddSubMessage(printer.ERROR, "Cloning of 'Automated Repo Setup' module failed", 1)
		}
	} else {
		statusPrinter.AddSubMessage(printer.SUCCESS, "'Automated Repo Setup' available", 1)
	}

	if !dirExists(repoEditorDir) {
		if cloneRepo("https://gitlab.com/divekit-tools/legacy/repo-editor.git", repoEditorDir) {
			statusPrinter.AddSubMessage(printer.SUCCESS, "'Repo Editor' was installed", 1)
		} else {
			modulesInstalled = false
			statusPrinter.AddSubMessage(printer.ERROR, "Cloning of 'Repo Editor' module failed", 1)
		}
	} else {
		statusPrinter.AddSubMessage(printer.SUCCESS, "'Repo Editor' available", 1)
	}

	if modulesInstalled {
		installingModulesMessage.Resolve()
		statusPrinter.PrintMainStatus()
	} else {
		installingModulesMessage.Resolve()
		statusPrinter.SetMainStatus(printer.ERROR)
		statusPrinter.PrintMainStatus()
		return fmt.Errorf("failed to install some modules")
	}

	return nil
}

func dirExists(path string) bool {
	info, err := os.Stat(path)
	if os.IsNotExist(err) {
		return false
	}
	return info.IsDir()
}

func cloneRepo(repoURL, clonePath string) bool {
	cmd := exec.Command("git", "clone", repoURL, clonePath)
	cmd.Stdout = io.Discard
	cmd.Stderr = io.Discard
	err := cmd.Run()
	if err != nil {
		printer.PrintStatus(printer.ERROR, fmt.Sprintf("Failed to clone repository: %s", repoURL), 0)
		return false
	}
	return true
}

func runNpmInstall(path string) bool {
	cmd := exec.Command("npm", "install")
	cmd.Stdout = io.Discard
	cmd.Stderr = io.Discard
	err := cmd.Run()
	if err != nil {
		printer.PrintStatus(printer.ERROR, fmt.Sprintf("Failed to run npm install in: %s", path), 0)
		return false
	}
	return true
}
