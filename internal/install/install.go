package install

import (
	"divekit/internal"
	"fmt"
	"os"
	"path/filepath"
	"runtime"
	"strings"

	config "gitlab.com/git.nrw/divekit/modules/config-management"

	"github.com/apex/log"
	"github.com/spf13/viper"
)

func copyFile(src, dst string) error {
	log.Debugf("Copying file from %s to %s", src, dst)
	sourceContent, err := os.ReadFile(src)
	if err != nil {
		return err
	}

	return os.WriteFile(dst, sourceContent, 0644)
}

func updateUnixPath(binDir string) error {
	homeDir, err := os.UserHomeDir()
	if err != nil {
		return err
	}

	log.Debug("Updating PATH in shell configuration files")

	// Check common shell config files
	shellConfigs := []string{
		filepath.Join(homeDir, ".bashrc"),
		filepath.Join(homeDir, ".zshrc"),
		filepath.Join(homeDir, ".profile"),
	}

	for _, configFile := range shellConfigs {
		if _, err := os.Stat(configFile); err == nil {
			log.Debugf("Checking shell config file: %s", configFile)

			// Read existing content
			content, err := os.ReadFile(configFile)
			if err != nil {
				log.WithError(err).Debugf("Failed to read config file: %s", configFile)
				continue
			}

			// Check if PATH update is already present
			if !strings.Contains(string(content), binDir) {
				// Append PATH update
				newContent := fmt.Sprintf("%s\nexport PATH=\"$PATH:%s\"\n", string(content), binDir)
				if err := os.WriteFile(configFile, []byte(newContent), 0644); err != nil {
					log.WithError(err).Debugf("Failed to update config file: %s", configFile)
					continue
				}
				log.Infof("Updated PATH in %s", configFile)
			} else {
				log.Debugf("PATH already updated in %s", configFile)
			}
		}
	}

	return nil
}

func SetupDivekitDirectories() error {
	// Create .divekit directory in user's home
	divekitHome, err := internal.GetDivekitHome()
	if err != nil {
		return fmt.Errorf("failed to create divekit home directory: %v", err)
	}
	log.Debug("Divekit home directory: " + divekitHome)

	// Create config directory and default config file
	configDir := filepath.Join(divekitHome, "config", "default")
	if err := os.MkdirAll(configDir, 0755); err != nil {
		return fmt.Errorf("failed to create config directory: %v", err)
	}
	log.Debug("Created config directory: " + configDir)

	// Create default config file
	defaultConfigPath := filepath.Join(configDir, "origin.json")
	if err := config.CreateDefaultConfigFile(defaultConfigPath); err != nil {
		return fmt.Errorf("failed to create default config: %v", err)
	}
	log.Debug("Created default config file: " + defaultConfigPath)

	return nil
}

func CreateDefaultEnvFile() error {
	divekitHome, err := internal.GetDivekitHome()
	if err != nil {
		return err
	}

	envPath := filepath.Join(divekitHome, ".env")
	if _, err := os.Stat(envPath); os.IsNotExist(err) {
		homeDir, err := os.UserHomeDir()
		if err != nil {
			return fmt.Errorf("failed to get user home directory: %v", err)
		}
		defaultMembersDir := filepath.Join(homeDir, ".divekit", "members")

		v := viper.New()
		v.SetConfigFile(envPath)
		v.SetConfigType("env")
		v.Set(config.MembersDirEnvVar, defaultMembersDir)

		if err := v.WriteConfig(); err != nil {
			return fmt.Errorf("failed to create .env file: %v", err)
		}
		log.Debug("Created .env file with default members directory")
	}
	return nil
}

func InstallExecutable(targetPath string) error {
	// Get the current executable path
	execPath, err := os.Executable()
	if err != nil {
		return fmt.Errorf("failed to get executable path: %v", err)
	}
	log.Debug("Current executable path: " + execPath)

	// Copy executable to target location
	if err := copyFile(execPath, targetPath); err != nil {
		return fmt.Errorf("failed to copy executable: %v", err)
	}
	log.Debug("Copied executable to target location")

	// Make the file executable on Unix systems
	if runtime.GOOS != "windows" {
		if err := os.Chmod(targetPath, 0755); err != nil {
			return fmt.Errorf("failed to make file executable: %v", err)
		}
		log.Debug("Made file executable")
	}

	return nil
}

func DetermineInstallationPath() (string, error) {
	if runtime.GOOS == "windows" {
		return getWindowsInstallPath()
	}
	return getUnixInstallPath()
}

// gets the installation path for windows (will be installed in C:\Users\<username>\AppData\Local\Microsoft\WindowsApps\divekit.exe)
func getWindowsInstallPath() (string, error) {
	localAppData := os.Getenv("LOCALAPPDATA")
	if localAppData == "" {
		return "", fmt.Errorf("LOCALAPPDATA environment variable not set")
	}
	targetPath := filepath.Join(localAppData, "Microsoft", "WindowsApps", "divekit.exe")
	log.Debug("Windows installation path: " + targetPath)
	return targetPath, nil
}

// gets the installation path for unix (will be installed in ~/bin/divekit)
func getUnixInstallPath() (string, error) {
	homeDir, err := os.UserHomeDir()
	if err != nil {
		return "", fmt.Errorf("failed to get user home directory: %v", err)
	}

	binDir := filepath.Join(homeDir, "bin")
	if err := os.MkdirAll(binDir, 0755); err != nil {
		return "", fmt.Errorf("failed to create bin directory: %v", err)
	}

	targetPath := filepath.Join(binDir, "divekit")
	log.Debug("Unix installation path: " + targetPath)

	// Add ~/bin to PATH if not already present
	if err := updateUnixPath(binDir); err != nil {
		log.WithError(err).Warn("Failed to update PATH")
		log.Warnf("Please add %s to your PATH manually", binDir)
	}

	return targetPath, nil
}
