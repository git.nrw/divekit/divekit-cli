#!/bin/bash

# Colors for better visibility
GREEN='\033[0;32m'
BLUE='\033[0;34m'
GRAY='\033[0;90m'
RED='\033[0;31m'
NC='\033[0m' # No Color

# Function to check if working directory is clean
check_working_directory() {
    if ! git diff-index --quiet HEAD -- || ! git diff-files --quiet; then
        echo -e "${RED}Error: Working directory is not clean. Please commit or stash changes first.${NC}"
        exit 1
    fi
}

# Function to get the latest tag for a repository
get_latest_tag() {
    cd "$1" || exit 1
    git fetch --tags > /dev/null 2>&1
    LATEST_TAG=$(git describe --tags --abbrev=0 2>/dev/null || echo "v0.0.0")
    cd - > /dev/null || exit 1
    echo "$LATEST_TAG"
}

# Function to create and push a new tag
create_tag() {
    local DIR=$1
    local NEW_VERSION=$2
    
    cd "$DIR" || exit 1
    
    echo -e "${GRAY}"
    # Delete tag if it exists
    git tag -d "$NEW_VERSION" 2>/dev/null
    git push --delete origin "$NEW_VERSION" 2>/dev/null
    
    # Create and push new tag
    git tag -a "$NEW_VERSION" -m "Release $NEW_VERSION"
    git push origin "$NEW_VERSION"
    git push
    echo -e "${NC}\n"
    
    cd - > /dev/null || exit 1
}

# Function to verify tag exists
verify_tag() {
    local REPO=$1
    local TAG=$2
    git ls-remote --tags "$REPO" | grep -q "refs/tags/$TAG"
    return $?
}

# Main script
check_working_directory

CURRENT_VERSION=$(get_latest_tag ".")
echo -e "${GREEN}Current Divekit version: ${BLUE}$CURRENT_VERSION${NC}"
read -p "Enter new version: " DIVEKIT_NEW

echo -e "\n${GREEN}Tagging modules:${NC}"

# Config Management
cd "pkg/config-management" || exit 1
check_working_directory
cd - > /dev/null || exit 1

CONFIG_CURRENT=$(get_latest_tag "pkg/config-management")
echo -e "${GREEN}- config-management${NC}"
echo -e "  Current version: ${BLUE}$CONFIG_CURRENT${NC}"
read -p "  Enter new version: " CONFIG_NEW

# Verify and create config-management tag
if create_tag "pkg/config-management" "$CONFIG_NEW"; then
    # Wait for tag to be available
    sleep 2
    if ! verify_tag "https://gitlab.com/git.nrw/divekit/modules/config-management.git" "$CONFIG_NEW"; then
        echo -e "${RED}Failed to verify config-management tag${NC}"
        exit 1
    fi
fi

# Gitlab Adapter
cd "pkg/gitlab-adapter" || exit 1
check_working_directory
cd - > /dev/null || exit 1

GITLAB_CURRENT=$(get_latest_tag "pkg/gitlab-adapter")
echo -e "${GREEN}- gitlab-adapter${NC}"
echo -e "  Current version: ${BLUE}$GITLAB_CURRENT${NC}"
read -p "  Enter new version: " GITLAB_NEW

# Verify and create gitlab-adapter tag
if create_tag "pkg/gitlab-adapter" "$GITLAB_NEW"; then
    # Wait for tag to be available
    sleep 2
    if ! verify_tag "https://gitlab.com/git.nrw/divekit/modules/gitlab-adapter.git" "$GITLAB_NEW"; then
        echo -e "${RED}Failed to verify gitlab-adapter tag${NC}"
        exit 1
    fi
fi

echo -e "${GRAY}"
# Update main project dependencies
echo "Updating dependencies in go.mod..."
go get "gitlab.com/git.nrw/divekit/modules/config-management@$CONFIG_NEW"
if [ $? -ne 0 ]; then
    echo -e "${RED}Failed to update config-management dependency${NC}"
    exit 1
fi

go get "gitlab.com/git.nrw/divekit/modules/gitlab-adapter@$GITLAB_NEW"
if [ $? -ne 0 ]; then
    echo -e "${RED}Failed to update gitlab-adapter dependency${NC}"
    exit 1
fi

go mod tidy
if [ $? -ne 0 ]; then
    echo -e "${RED}Failed to tidy go.mod${NC}"
    exit 1
fi

# Verify that the correct versions are in go.mod
if ! grep -q "gitlab.com/git.nrw/divekit/modules/config-management $CONFIG_NEW" go.mod; then
    echo -e "${RED}Error: config-management version in go.mod does not match $CONFIG_NEW${NC}"
    exit 1
fi

if ! grep -q "gitlab.com/git.nrw/divekit/modules/gitlab-adapter $GITLAB_NEW" go.mod; then
    echo -e "${RED}Error: gitlab-adapter version in go.mod does not match $GITLAB_NEW${NC}"
    exit 1
fi

# Commit and tag main project
git add go.mod go.sum
git commit -m "chore: update dependencies and prepare $DIVEKIT_NEW release"
git tag -a "$DIVEKIT_NEW" -m "Release $DIVEKIT_NEW"
git push origin "$DIVEKIT_NEW"
git push
echo -e "${NC}\n"

echo -e "${GREEN}Successfully tagged Divekit to version ${BLUE}$DIVEKIT_NEW${NC}!\n" 