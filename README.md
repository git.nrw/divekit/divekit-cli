# DIVEKIT CLI <!-- omit in toc -->

- [Prerequisites](#prerequisites)
- [Development](#development)
  - [Set up your development environment by following these steps:](#set-up-your-development-environment-by-following-these-steps)
  - [Error Handling](#error-handling)
    - [Example Usage](#example-usage)
- [`divekit patch` (deprecated)](#divekit-patch-deprecated)
  - [Assumptions for using the CLI `patch` command](#assumptions-for-using-the-cli-patch-command)
  - [What the Patch Tool does](#what-the-patch-tool-does)
  - [How to call the Patch Command via CLI](#how-to-call-the-patch-command-via-cli)
  - [Documentation for flags and parameters](#documentation-for-flags-and-parameters)
  - [Development of the "patch" subcommand - how to test](#development-of-the-patch-subcommand---how-to-test)
- [Glossary](#glossary)
    - [Project](#project)
    - [Origin](#origin)
    - [Remotes](#remotes)
    - [Members](#members)
    - [Distribution](#distribution)

## Prerequisites

-   Command Line access
-   Internet connection
-   [Go 1.23](https://golang.org/dl/) or higher
-   Gitlab
    -   Access Token
    -   Group IDs
-   (Git)
-   (npm)


## Development

### Set up your development environment by following these steps:

1. **Clone the repository**:

```bash
git clone https://gitlab.com/git.nrw/divekit/tools/divekit-cli.git
```

2. **Navigate to the project directory**:

```bash
cd divekit-cli
```

3. **Install the required dependencies**:
```bash
go mod download
```

install local modules (later possibly optional - but for development a huge help):
  
```bash
mkdir pkg
cd pkg
git clone https://gitlab.com/git.nrw/divekit/modules/gitlab-adapter
git clone https://gitlab.com/git.nrw/divekit/modules/config-management

cd ..
go work init
go work use ./pkg/gitlab-adapter
go work use ./pkg/config-management
```


4. **Build the CLI**:

```bash
chmod +x build.sh
./build.sh
```
Then answer the questions or just press Enter for the default values (windows, amd64).

This will create a `divekit` executable in the `bin` directory. You can run this executable from the command line to use the CLI or run `install` on it to install it globally.

For Example:

```bash
./bin/divekit_windows_amd64.exe install
```

This will install the `divekit` command globally on your system. You can now run `divekit` from any directory.

5. **Run the CLI**:

```bash
./bin/divekit_windows_amd64.exe

# or

divekit
```

...or if you want to execute directly from the source code:

```bash
go run cmd/divekit/main.go
```

6. **Run the tests**:

```bash
go test ./...
```

7. **Make your changes and submit a merge request**.

### Error Handling

The project implements a structured error handling system that distinguishes between critical and non-critical errors. This pattern is currently implemented in the `distribute` package and can serve as a template for other packages.

#### Error Pattern

Each package can define its own error types and handling behavior. The pattern consists of:

1. A custom error type that implements the `error` interface
2. Specific error types as constants
3. Methods to determine error severity and behavior

Example from the distribute package:

```go
// Custom error type
type CustomError struct {
    ErrorType ErrorType
    Message   string
    Err       error
}

// Error types
const (
    // Critical errors that lead to termination
    ErrConfigLoad       // Configuration loading errors
    ErrWorkingDir      // Working directory access errors
    
    // Non-critical errors that trigger warnings
    ErrMembersNotFound // Member lookup failures
)
```

#### Example Implementation

Here's how to implement this pattern in your package:

```go
// Create a new error
if err := loadConfig(); err != nil {
    return NewCustomError(ErrConfigLoad, "failed to load configuration", err)
}

// Handle non-critical errors
if err := validateData(); err != nil {
    if !err.IsCritical() {
        log.Warn(err.Error())
        // Continue execution...
    } else {
        return err
    }
}
```

#### Error Behavior

Each package can define its own error behavior, but should follow these general principles:

- **Critical Errors**: Should terminate the current operation
- **Non-Critical Errors**: Should generate warnings but allow continuation
- **Wrapped Errors**: Should preserve the original error context

Each error should include:
- An error type indicating its severity
- A descriptive message
- The original error (if applicable)
- A method to determine if it's critical

This pattern provides consistent error handling while remaining flexible enough to accommodate different package requirements. The `distribute` package provides a reference implementation of this pattern.




## `divekit patch` (deprecated)

**This is work in progress, but the Patch Tool (`divekit patch` command) is robust and usable.**
No other subcommands are usable as of now.

Learn more about the CLI and other divekit tools in the [Divekit documentation](https://divekit.github.io/docs/cli/)
... but the documentation there is outdated with regard to the CLI. So this README acts as a temporary documentation,
to be migrated later.

### Assumptions for using the CLI `patch` command

The CLI assumes the following aspects to work properly:

-   The following repos need to be cloned under **their original names**, and in the same parent "git" directory:
    -   [divekit-cli](https://github.com/divekit/divekit-cli)
    -   [divekit-automated-repo-setup](https://github.com/divekit/divekit-automated-repo-setup)
    -   [divekit-repo-editor](https://github.com/divekit/divekit-repo-editor)
-   You need to have `npm` installed on your machine

That's it. Neither a specific IDE nor Go need to be installed.

### What the Patch Tool does

The advantage of the patch tool is that you omit all the error-prone manual copy-pasting between two tools
(ARS and Repo Editor). The CLI command `divekit patch` automatically performs the following steps:

1. Read all necessary settings from the origin repo (see glossary below)
2. Define the config settings for ARS, based on input flags / parameters and the origin repo information
3. Run the [ARS](#ars) "locally" to produce the individualized patch files in the local file system
4. Copy the localized files in the appropriate input directories in the [Repo Editor](#repo-editor)
5. Configure the Repo Editor settings based on the input flags / parameters and the origin repo information
6. Run the Repo Editor

### How to call the Patch Command via CLI

Let's assume that you want to patch buggy unit test file called `E2WhateverTests.java`, and the `pom.xml`
where a dependency is missing. Your origin repo is called `st2-m3-origin`.
Let's further assume that you have two [distributions](#distribution):

-   The standard "milestone" distribution, which contains all the student campus ids and UUIDs, and
    the Gitlab groups for the students.
-   A "test" distribution with just two repos, using supervisor campus ids.

As a first step, open a shell and go to your `divekit-cli` repo dir. (You can run the command from any other location,
just make sure you have divekit.exe in your path, e.g. by copying it somewhere.)

It is recommended to do a trial run with the "test" distribution, where you patch only the two
supervisors' repos. This is done by the following command:

```
divekit patch -m <my-local-git-dir> -o st2-m3-origin -d test E2WhateverTests.java pom.xml`
```

Note that you don't have to specify the precise path of your files. The patch tool searches relevant locations
in the origin repo to find the full path, and gives an error if there are multiple files matching that name. You
should check in the supervisors' repos if the patch delivered the expected results.

If this was successful, you can patch the student repos:

```
divekit patch -m <my-local-git-dir> -o st2-m3-origin E2WhateverTests.java pom.xml`
```

The only difference is that the `-d test` option is missing - the tool assumes that the student distribution
is called `milestone`. If it has a different name, just use `-d` accordingly.

### Documentation for flags and parameters

The best way is to call `divekit patch -h`, then you get a brief documentation of available flags.


### Development of the "patch" subcommand - how to test

(this is just temporary during development, will be removed later)

As test data, I have used the following origin repo:

-   st2-m0-origin (https://git.st.archi-lab.io/staff/st2/ss22/m0/st2-m0-origin)
    -   In the .divekit_norepo, the necessary files have been added
    -   There are two test files, one open and one hidden (test repo only). I have added a useless (but not
        confusing) individualized comment on top of the first test in each test file.



## Glossary

Let me help improve these definitions to make them clearer and more precise:

#### Project

A project is a repository on GitHub. It serves as the top-level container for all code, issues, documentation, and related resources. In its simplest form, a project can be thought of as a directory containing files and their version history.

#### Origin

An origin is a repository that has been initialized using the `divekit init` command. It must contain all required configuration files, properly structured within a `.divekit` directory at the repository's root level. This origin repository serves as the template from which individual distributions are created.

#### Remotes

Remotes are the distributed repositories that are created for individual users during the distribution process. These repositories are tracked in the origin repository within `.divekit/distributions/<distribution>/remotes.json`. Each remote represents a personalized copy of the project for a specific user.

#### Members

Members are the users for whom individualized repositories are created. The member list is stored in the directory specified by the `$DIVEKIT_MEMBERS` environment variable. If no custom location is specified, the default path is `~/.divekit/members`. This list determines who will receive a personalized remote repository during distribution.

#### Distribution

A distribution is a set of personalized repositories created for specific users based on the origin repository. Each distribution is defined by a configuration file stored in the `.divekit/distributions` directory within the origin repository. The configuration file specifies the members who will receive individualized repositories and the remotes that will be created for them.

You can name your distributions as you like, but we suggest this convention:

-   `sandbox`: for testing purposes, using the supervisor(s) campusIDs, creating repos in the `staff` group
-   `ms1`, `ms2`, `ms3`, ...: for the actual students, creating repos in the `student` group

```json
.divekit/
    distributions/
        ms1/
            config.json
            // individual_repositories_03-04-2023 01-39-08.json
            remotes.json
        ms2/
            config.json
            // individual_repositories_03-04-2023 01-39-08.json
            remotes.json
        sandbox/
            config.json
            // individual_repositories_test_ecom.json
            remotes.json
```


