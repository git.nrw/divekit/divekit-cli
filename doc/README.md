# Divekit CLI

## Commands
- [`divekit install`](<commands/divekit install.md>) (install divekit)
- [`divekit init`](<commands/divekit init.md>) (inititalize origin)
- [`divekit doctor`](<commands/divekit doctor.md>) (prepare/install)
  - `divekit doctor list` (list available rules)
  - `divekit doctor check <validation-keys>` (check specific validation rules)
- [`divekit distribute`](<commands/divekit distribute.md>) (distribute repositories to gitlab)
  - `divekit distribute --distribution <distribution>`
- [`divekit patch`](<commands/divekit patch.md>) (edit repositories)
  - `divekit patch --distribution <distribution> <file1> <file2> ...`
- [`divekit update`](<commands/divekit update.md>) (update divekit)
  - `divekit update -y`


## Steps

1. Define configs
  1. Define Minimal-Origin-Config v2 (only need a few things for now)
  2. Define user data config
2. Expand `divekit doctor`
  1. `divekit doctor --glpat <TOKEN>`
  2. or ~`echo 'export MY_VAR="value"' >> ~/.bashrc` and `source ~/.bashrc`
3. Implement `divekit init` (sets origin configs)
4. Implement `divekit distribute` (distributes repos)
5. Implement `divekit patch` (without ARS and Repo Editor)
6. Expand `divekit distribute` and enable customization
7. Implement `divekit install`
8. Implement `divekit update`
9. Implement `divekit doctor` (validates origin and remotes)

## Open Questions
- For `divekit distribute --distribution "supervisor"`, should repos be created only for existing members? Or also for non-existing ones?
  - Is that what `repositoryCount` is for?
- Cache in `.divekit_norepo/cache` or `$HOME/.divekit/cache`? (`cache` -> `stage`?)

## Comments

### 2024-10-09 Stefan, Torben (via Zoom)
- Import of latecomers in groups (full CSV with **all** members)
  - flag `--replace` for overwriting existing members, otherwise merge
- maybe implement `.divekitignore` for files that should not be distributed (and keep the `*_norepo` functionality)

### 2024-09-12 Stefan, Fabian, Torben (in Person)
- Automatically use origin repo name for subfolders in tests
- Overwriting potentially problematic due to "pending delete" (GitLab)
- Undefined commands like `divekit check` -> Error with "Did you mean `divekit origin check`?"

Backlog/Off-Topic:
- `divekit remote doctor` / student doctor "changes to pom.xml"
- "last build failed" -> Display for student - on the reporting page?