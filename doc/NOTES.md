# Notes

### 🚧 divekit distribute

Current state:

```
divekit setup -m <HomeDir> -o <Origin> --details --distribution <distribution>
```

- `-m <HomeDir>` - HomeDir
- `-o <Origin>`  - relative to HomeDir
- `--details` - shows the details
- `--distribution <test|live|...(distribution)>` - specifies which distribution should be created

So, I have to perform such an acrobatic act to properly connect the divekit-cli:
```bash
-m "C:\Users\tbuck\dev\_repos\TH Köln\Divekit\staff\divekit-modules" 
-o "\..\origins\test-origin-1"
```

in repositoryConfig.json:
```json
{
    //...
    "repository": {
        "repositoryName": "GO-CLI-WS{{now \"2006\"}}-{{uuid}}",
        "repositoryCount": 1,
        "repositoryMembers": [
            ["ada", "charles"],
            ["jobs", "woz"]
        ]
    },
    // ...
}
```

```bash
go run main.go setup -m "C:\Users\tbuck\dev\_repos\TH Köln\Divekit\staff\divekit-modules" -o "\..\origins\test-origin-1" --details --distribution "test" -t "{GITLAB_TOKEN}" --remote "https://git.archi-lab.io/" --dry-run
```
