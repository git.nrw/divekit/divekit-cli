# Config Redesign

## Bisheriger Zustand

### ARS

-   [`{ARS}/.env`](/config-redesign/ARS/env.md) [>> INIT](#0-init)
-   [`{ARS}/originRepositoryConfig.json`](/config-redesign/ARS/originRepositoryConfig.json.md) [>> INIT](#0-init)
-   [`{ARS}/relationsConfig.json`](/config-redesign/ARS/relationsConfig.json.md) [>> INIT](#0-init)
-   [`{ARS}/variationsConfig.json`](/config-redesign/ARS/variationsConfig.json.md) [>> SEMESTER](#1-semester)
-   [`{ARS}/repositoryConfig.json`](/config-redesign/ARS/meilenstein/repositoryConfig.json.md) [>> MILESTONE](#2-milestone)
-   [`{ARS}/variableExtensionsConfig.json`](/config-redesign/ARS/variableExtensionsConfig.json.md) ( [>> INIT](#0-init) )
    -   `$.[i].variableExtensions.ClassPath.preValue` [>> SEMESTER](#1-semester)

### RepoEditor (-> PatchTool)

-   [`{RepoEditor}/editorConfig.json`](/config-redesign/RepoEditor/editorConfig.json.md) [>> CALL](#3-call)

### OriginRepo

-   [`{OriginRepo}/repositoryConfig.json`](/config-redesign/OriginRepo/repositoryConfig.json.md)
    -   `$.general`
        -   `localMode` [>> CALL](#3-call)
        -   `createTestRepository` [>> INIT](#0-init)
        -   `variateRepositories` [>> INIT](#0-init)
        -   `deleteSolution` [>> CALL](#3-call)
        -   `activateVariableValueWarnings` [>> INIT](#0-init)
        -   `maxConcurrentWorkers` [>> INIT](#0-init)
        -   `globalLogLevel` [>> INIT](#0-init)
    -   `$.repository`
        -   `repositoryName` [>> CALL](#3-call)
        -   `repositoryCount` [>> INIT](#0-init)
        -   `repositoryMembers` [>> MILESTONE](#2-milestone)
    -   `$.individualRepositoryPersist`
        -   `useSavedIndividualRepositories` [>> CALL](#3-call)
        -   `savedIndividualRepositoriesFileName` [>> CALL](#3-call)
    -   `$.local`
        -   `originRepositoryFilePath` [>> MILESTONE](#2-milestone)
        -   `subsetPaths` [>> CALL](#3-call)
    -   `$.remote`
        -   `originRepositoryId` [>> MILESTONE](#2-milestone)
        -   `codeRepositoryTargetGroupId` [>> MILESTONE](#2-milestone)
        -   `testRepositoryTargetGroupId` [>> MILESTONE](#2-milestone)
        -   `deleteExistingRepositories` [>> CALL](#3-call)
        -   `addUsersAsGuests` [>> CALL](#3-call)
    -   `$.overview`
        -   `generateOverview` [>> INIT](#0-init)
        -   `overviewRepositoryId` [>> SEMESTER](#1-semester)
        -   `overviewFileName` [>> MILESTONE](#2-milestone)

<!--
[>> INIT](#0-init)
[>> SEMESTER](#1-semester)
[>> MILESTONE](#2-milestone)
[>> CALL](#3-call)
-->

## Zugeordnete Konfigurationen

### [0] INIT

Konfigurationen die üblicherweise nur einmal bei der Installation definiert werden müssen.

**Optimalerweise in**: `{$HOME}/.divekit/`

-   [`{ARS}/.env`](/config-redesign/ARS/env.md) [>> INIT](#0-init)
-   [`{ARS}/originRepositoryConfig.json`](/config-redesign/ARS/originRepositoryConfig.json.md) [>> INIT](#0-init)
-   [`{ARS}/relationsConfig.json`](/config-redesign/ARS/relationsConfig.json.md) [>> INIT](#0-init)
-   [`{ARS}/variableExtensionsConfig.json`](/config-redesign/ARS/variableExtensionsConfig.json.md) ( [>> INIT](#0-init) )
    -   `$.[i].variableExtensions.ClassPath.preValue` [>> SEMESTER](#1-semester)
-   [`{OriginRepo}/repositoryConfig.json`](/config-redesign/OriginRepo/repositoryConfig.json.md)
    -   `$.general`
        -   `createTestRepository` [>> INIT](#0-init)
        -   `variateRepositories` [>> INIT](#0-init)
        -   `activateVariableValueWarnings` [>> INIT](#0-init)
        -   `maxConcurrentWorkers` [>> INIT](#0-init)
        -   `globalLogLevel` [>> INIT](#0-init)
    -   `$.repository.repositoryCount` [>> INIT](#0-init)
    -   `$.overview.generateOverview` [>> INIT](#0-init)

### [1] SEMESTER

Konfigurationen die üblicherweise nur ungefähr einmal pro Semester definiert werden müssen.
Sie sind am besten im OriginRepo untergebracht.

**Optimalerweise in**: `{OriginRepo}/.divekit_norepo/{distribution}/`

-   [`{ARS}/variationsConfig.json`](/config-redesign/ARS/variationsConfig.json.md) [>> SEMESTER](#1-semester)
-   [`{OriginRepo}/repositoryConfig.json`](/config-redesign/OriginRepo/repositoryConfig.json.md)
    -   `$.overview.overviewRepositoryId` [>> SEMESTER](#1-semester)

### [2] MILESTONE

Konfigurationen die üblicherweise nur ungefähr einmal pro Meilenstein definiert werden müssen. Sie sind am besten im OriginRepo untergebracht.

**Optimalerweise in**: `{OriginRepo}/.divekit_norepo/{distribution:{milestone}}/`

-   [`{ARS}/repositoryConfig.json`](/config-redesign/ARS/meilenstein/repositoryConfig.json.md) [>> MILESTONE](#2-milestone)
-   [`{OriginRepo}/repositoryConfig.json`](/config-redesign/OriginRepo/repositoryConfig.json.md)
    -   `$.repository.repositoryMembers` [>> MILESTONE](#2-milestone)
    -   `$.local.originRepositoryFilePath` [>> MILESTONE](#2-milestone)
    -   `$.remote`
        -   `originRepositoryId` [>> MILESTONE](#2-milestone)
        -   `codeRepositoryTargetGroupId` [>> MILESTONE](#2-milestone)
        -   `testRepositoryTargetGroupId` [>> MILESTONE](#2-milestone)
    -   `$.overview.overviewFileName` [>> MILESTONE](#2-milestone)

### [3] CALL

Konfigurationen die bei jedem Aufruf definiert werden müssen.

**Optimalerweise in**: `CLI flags`

-   [`{RepoEditor}/editorConfig.json`](/config-redesign/RepoEditor/editorConfig.json.md) [>> CALL](#3-call)
-   [`{OriginRepo}/repositoryConfig.json`](/config-redesign/OriginRepo/repositoryConfig.json.md)
    -   `$.general`
        -   `localMode` [>> CALL](#3-call)
        -   `deleteSolution` [>> CALL](#3-call)
    -   `$.repository.repositoryName` [>> CALL](#3-call)
    -   `$.individualRepositoryPersist`
        -   `useSavedIndividualRepositories` [>> CALL](#3-call)
        -   `savedIndividualRepositoriesFileName` [>> CALL](#3-call)
    -   `$.local.subsetPaths` [>> CALL](#3-call)
    -   `$.remote`
        -   `deleteExistingRepositories` [>> CALL](#3-call)
        -   `addUsersAsGuests` [>> CALL](#3-call)

## Result


### Minimal Config
`~/.divekit/.env`
```env
API_TOKEN=YOUR_ACCESS_TOKEN
DEFAULT_BRANCH=main
```

`{ORIGIN}/.divekit/distributions/<distribution>/config.json`
```json
{
    "version": "2.0",
    "remotes": {
        "name": "GO-CLI-WS{{now 2006}}-{{uuid}}",
        "groupIds": {
            // "origin": 123456, // why is this needed?
            "code": 234567,
            "test": 345678
        },
        "members": "~/.divekit/members/divekit-members-0192aebf-414b-7141-95f7-268da1945cb5.json"
    },
}
```

`~/.divekit/config/<distribution>/members.csv`
```csv
username
tbuck
ada
charles
jobs
woz
```

`~/.divekit/members/divekit-members-0192aebf-414b-7141-95f7-268da1945cb5.json`
```json
{
    "members": {
        "bc230991-f0b3-4ed4-a6d2-fe5c9e8d88f5": ["tbuck"],
        "1de47b3f-5a47-49a9-8550-1f16806e7570": ["ada"],
        "81d06a81-7989-4896-84f5-c0451c88319e": ["charles"],
        "67dd7e13-8489-47be-a371-83f50ee0b975": ["jobs"],
        "5f476897-7cf7-4b12-9e4e-d5e3d5db195e": ["woz"]
    }
}
```

### Future

#### [0] INIT

[`{ARS}/.env`](/config-redesign/ARS/env.md) wird in `{$HOME}/.divekit/` abgelegt

```
ACCESS_TOKEN=YOUR_ACCESS_TOKEN
HOST=https://git.st.archi-lab.io
BRANCH=main
```

---

[`{ARS}/originRepositoryConfig.json`](/config-redesign/ARS/originRepositoryConfig.json.md) -> `{$HOME}/.divekit/config/defaults/origin.json`

Wird hier bei Installation abgelegt und dann bei `divekit init` in die neuen Origin Repos kopiert.

```json
{
    "variables": {
        "variableDelimiter": "$"
    },
    "solutionDeletion": {
        "deleteFileKey": "//deleteFile",
        "deleteParagraphKey": "//delete",
        "replaceMap": {
            "//unsup": "throw new UnsupportedOperationException();",
            "//todo": "// TODO"
        }
    },
    "warnings": {
        "variableValueWarnings": {
            "typeWhiteList": ["json", "java", "md"],
            "ignoreList": ["name", "type"]
        }
    }
}
```

Änderungsvorschlag:

```json
{
    "version": "2.0",
    "variables": {
        "delimiter": "$"
    },
    "solutionCleanup": {
        "deleteFile": "//deleteFile",
        "replaceParagraph": {
            "//unsup": "throw new UnsupportedOperationException();",
            "//todo": "// TODO",
            "//delete": null
        }
    },
    "warnings": {
        "variation": {
            "fileTypes": ["json", "java", "md"],
            "ignore": ["name", "type"]
        }
    }
}
```

---

`{ARS}/relationsConfig.json` -> `{$HOME}/.divekit/config/defaults/relations.json`

Wird hier bei Installation abgelegt und dann bei `divekit init` in die neuen Origin Repos kopiert.

> [!NOTE]  
> Ich verstehe nicht 100% wofür das ist - bleibt möglicherweise für immer so und muss nicht in die Origin Repo kopiert werden?  
> (was ist UmletRev? Was bedeutet der Stern?)

```json
[
    {
        "id": "OneToOne",
        "Umlet": "lt=-\nm1=1\nm2=1",
        "UmletRev": "lt=-\nm1=1\nm2=1",
        "Short": "1 - 1",
        "Description": "one to one"
    },
    {
        "id": "OneToMany",
        "Umlet": "lt=-\nm1=1\nm2=*",
        "UmletRev": "lt=-\nm1=*\nm2=1",
        "Short": "1 - n",
        "Description": "one to many"
    },
    {
        "id": "ManyToOne",
        "Umlet": "lt=-\nm1=*\nm2=1",
        "UmletRev": "lt=-\nm1=1\nm2=*",
        "Short": "n - 1",
        "Description": "many to one"
    },
    {
        "id": "ManyToMany",
        "Umlet": "lt=-\nm1=*\nm2=*",
        "UmletRev": "lt=-\nm1=*\nm2=*",
        "Short": "n - m",
        "Description": "many to many"
    }
]
```

Änderungsvorschlag:

-   `id`->`key` ?

```json
{
    "version": "2.0",
    "relations": [
        {
            "id": "OneToOne",
            "umlet": "lt=-\nm1=1\nm2=1",
            "umletRev": "lt=-\nm1=1\nm2=1",
            "short": "1 - 1",
            "description": "one to one"
        },
        {
            "id": "OneToMany",
            "umlet": "lt=-\nm1=1\nm2=*",
            "umletRev": "lt=-\nm1=*\nm2=1",
            "short": "1 - n",
            "description": "one to many"
        },
        {
            "id": "ManyToOne",
            "umlet": "lt=-\nm1=*\nm2=1",
            "umletRev": "lt=-\nm1=1\nm2=*",
            "short": "n - 1",
            "description": "many to one"
        },
        {
            "id": "ManyToMany",
            "umlet": "lt=-\nm1=*\nm2=*",
            "umletRev": "lt=-\nm1=*\nm2=*",
            "short": "n - m",
            "description": "many to many"
        }
    ]
}
```

---

`{ARS}/variableExtensionsConfig.json` -> `{$HOME}/.divekit/config/defaults/variableExtensions.json`

Wird hier bei Installation abgelegt und dann bei `divekit init` in die neuen Origin Repos kopiert.

```json
[
    {
        "id": "Basic",
        "variableExtensions": {
            "": {
                "preValue": "",
                "value": "id",
                "postValue": "",
                "modifier": "NONE"
            },
            "Class": {
                "preValue": "",
                "value": "id",
                "postValue": "",
                "modifier": "NONE"
            },
            "Package": {
                "preValue": "",
                "value": "Class",
                "postValue": "",
                "modifier": "ALL_LOWER_CASE"
            },
            "ClassPath": {
                "preValue": "thkoeln.st.st2praktikum.racing.", // ??? deprecated ???
                "value": "Class",
                "postValue": ".domain",
                "modifier": "ALL_LOWER_CASE"
            }
        }
    },
    {
        "id": "Getter",
        "variableExtensions": {
            "GetToOne": {
                "preValue": "get",
                "value": "Class",
                "postValue": "",
                "modifier": "NONE"
            },
            "GetToMany": {
                "preValue": "get",
                "value": "s",
                "postValue": "",
                "modifier": "NONE"
            }
        }
    }
]
```

### Fragen

#### Aus meinen Notizen
Ich dachte ich hätte das schon irgendwo geschrieben, aber ich finde es nicht mehr.

- [0] INIT -> "Installation" gibt es 2x
    - Einmal bei der Installation von DiveKit
    - Einmal bei der Initialisierung von DiveKit in einem neuen OriginRepo  
  
Was sollte also wo hin (habe Ideen)?

- Wird die `preValue` noch gebraucht?  
Ich weiß leider nicht mehr was/warum genau, aber das problematisierte irgendetwas deutlich.

<!--
- XYZ?
  - [ ] Ja
  - [ ] Nein
  - Kommentar:
-->
