# divekit patch

> [!NOTE]
> Currently still based on ARS and the Repo-Editor

## Beispiel Ablauf (Vorschlag)
```bash
$ divekit patch --distribution "supervisor" E2WhateverTests.java pom.xml 

? Please type your commit message [Patch applied on 2024-10-04 08:42]: make some tests optional

Following repositories will be updated:
[√] (215x) supervisor::ST2-2024-{uuid}
[√] (215x) supervisor::ST2-2024-{uuid}-test

? Continue? [Y/n]: y

Updating repositories:
[███████████                                ] 42% (90/215)
```

---

```bash
$ divekit patch E2WhateverTests.java pom.xml 

? Found several distributions. Please choose one:
[x] local
[ ] supervisor
[ ] student

Following repositories will be updated:
[√] (215x) local::ST2-2024-{uuid}
[√] (215x) local::ST2-2024-{uuid}-test

? Continue? [Y/n]: y

Updating repositories:
[███████████                                ] 42% (90/215)
```
## Comments (DESC)
### 2024-10-01 Stefan, Torben (via Discord)
- Individual files are passed to the command
- Local testing is important for verification
- Variables are also replaced during patching
- Files are currently patched individually ([can also be done in one commit](/_meta/gitlab-api-upsert-files.md))

### 2024-09-12 Stefan, Fabian, Torben (in person)
- Distribution "test" -> "supervisor"
- Distribution "code" -> "student"

