# divekit config

```bash
$ divekit config

Usage:
  divekit config <command> <flags>

Available Commands:
    list         List configuration values
    get          Get configuration values
    set          Set configuration values
    unset        Unset configuration values

Flags:
    -h, --help   help for config
    -g, --global use global configuration
```

## Examples

### Listing all configuration values
```bash
divekit config list

You can get single values by calling
`$ divekit config get <dot-notated-path>`

Example:
`$ divekit config get origin.remotes.name

STM2-{{uuid}}
`

origin        - the origin configuration
  version     - the version of the origin configuration
  remotes     - information about the distributed remotes
    name      - the name of the remote with template variables
    groupIds  - the group IDs of the remote with template variables
      main    - the main group ID
      test    - the test group ID
  membersPath - the path to the members file
```

### Importing members from a CSV file
```bash
divekit config set origin.membersPath --import /path/to/members.csv
```
This command imports the members from the specified CSV file. The CSV should contain usernames (e.g., campusIDs) in the first column.

#### Import latecomers from a CSV file
```bash
divekit config set origin.membersPath --import /path/to/members.csv 
```
#### Import and overwrite existing members
```bash
divekit config set origin.membersPath --import /path/to/members.csv --replace
```

### Adding individual members
```bash
divekit config set origin.membersPath --add john.doe,jane.smith
```
This command adds two new members (John Doe and Jane Smith) to the existing list of members.

### Adding a group of members
```bash
divekit config set origin.membersPath --add-groups team-a:alice.jones,bob.wilson
```
This command adds a new group named "team-a" with two members (Alice Jones and Bob Wilson).

### Distributing repositories for latecomers
```bash
divekit distribute
```
This command distributes repositories for members who were added after the initial distribution.  
(`--sync`, `--refresh`, or just no flags if we know which ones are missing)