# divekit check

`divekit check` ist eine schnelle, weniger detailreiche Überprüfung als `divekit doctor`.
Es prüft die Umgebung auf häufige Probleme und gibt eine kurze Zusammenfassung aus.

## Beispielablauf
```bash
$ divekit check no-change-files --distibution <distribution> 

[√] All NoChange files are equal to local files
```

```bash
$ divekit check remotes --distibution <distribution> 

[√] All remotes are reachable
```


## Kommentare

### 2024-10-04 Torben
- Ist `divekit check` als eigener Befehl sinnvoll? (`divekit doctor --check-only`? `divekit doctor --only system`)
- Die Benennung der checks muss dann klar definiert werden - könnte im `divekit doctor`-Befehl passieren

### 2024-09-12 Stefan, Fabian, Torben (in Person)

- über origin drüber laufen und fehler suchen (wie `$TEST`, )
- "car" statt "vehicle" kommt potentiell vor
- Fehlendes Readme
- pmd-Datei (für code quality check) - mal mit mal ohne
  - in pipeline
  - in pom.xml
  - braucht eigene config
- Lösung?: ist pmd-config vorhanden? Wenn vorhanden, ist in pom.xml eintrag für pmd, dann automatisch reinsetzen oder warnen und automatisch für das ausrollen die pipeline anpassen
- standard pmd in {$HOME}/.divekit ?
  
- potentiell `divekit distribute --check`
- `divekit check`?
- `divekit origin check`?
 
vor anderen passenden Befehlen ausführen und ggf. abbrechen