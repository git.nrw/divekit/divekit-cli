# divekit overview

```bash
$ divekit overview --distribution students

Using:
- {DIVEKIT_HOME}/members/divekit-members-8125814e-01da-42dd-8be3-29df5dcd760e.json

Serving on http://localhost:8080

Opening browser...
```
Depending on how the current overview file is used, the html could be created dynamically.

...or it could be stored as html and/or markdown in the origin repo, next to the files that store the members.