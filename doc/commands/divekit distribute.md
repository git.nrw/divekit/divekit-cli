# divekit distribute 

> [!WARNING]
> was originally called `divekit setup` and was renamed because setup sounds like local preparation and not like distribution across multiple repositories.

Creates multiple repositories on GitLab based on the configurations in `repositoryConfig.json`.

> [!NOTE]
> Only partially functional - style still different  
> - Only creates repos with members
>   - no test repos
>   - no overview
> - Members are assigned directly
> - Members are checked but simply ignored


## Beispiel Ablauf
```bash
$ divekit distribute

? Found several distributions. Please choose one:
[ ] local
[x] supervisor
[ ] student

Checking members:
[√] 2 user available 
[X] 3 users not found:
    - ada
    - charles
    - jobs

Would create 2 repository with name "ST2-2024-{uuid}" and assign 2 members.

? Continue? [Y/n]: y

Creating main repositories at #234567:
[██████████████████████████████████████████████████] 100% (2/2)

Creating test repositories at #345678:
[██████████████████████████████████████████████████] 100% (2/2)

Assigning members:
[███████████████████████████                         ] 50% (1/2)

```

## Comments (DESC)
### 2024-10-01 Stefan, Torben (via Discord)
- Not `push` because:
  - `git push`
    - Performs consistency checks (merge needed, missing pulls)
    - There are differences between Origin and Remote (variables)
    - The target is not the client but a creation operation within the server (?)

### 2024-09-12 Stefan, Fabian, Torben (in person)
- `push` -> `create`?
- `push` -> `distribute`! (favorite)
