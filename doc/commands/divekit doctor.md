# divekit doctor (was "divekit init")

> [!WARNING]
> was originally called `divekit init` and should be renamed,
> because other functionality might be expected due to `git init` and `npm init`.

`divekit doctor` is an in-depth diagnosis of the entire environment. It provides a comprehensive analysis and detailed information about potential issues.
It is a kind of "first-aid tool" that offers concrete solutions or even automatically fixes problems.

Flutter uses `flutter doctor` for similar functionality, so `divekit doctor` could be appropriate.

> [!NOTE]
> Works, only token verification is still missing.

## divekit doctor (some checks failed)
```bash
$ divekit doctor

System:
[√] git available
[√] npm available
[√] Modules installed
    • 'Automated Repo Setup' available
    • 'Repo Editor' available
[X] Token not found
    • Please provide a GitLab API token via `divekit doctor --glpat <YOUR_TOKEN>`
    • You can create a new token at https://gitlab.git.nrw/-/user_settings/personal_access_tokens?name=git.nrw/divekit&scopes=api

Origin:
[√] Config available and valid
[√] No orphan variables found
[√] No hardcoded variations found

Distribution:
• supervisor:
    [√] All remotes are reachable
    [√] All NoChange files are equal to local files
• students:
    [!] No remotes found
        • Run `divekit distribute --distribution students` to distribute the repositories
```

## divekit doctor (all checks passed)
```bash
$ divekit doctor

System:
[√] git available
[√] npm available
[√] Modules installed
    • 'Automated Repo Setup' available
    • 'Repo Editor' available
[√] Token is valid and has the necessary permissions

Origin:
[√] Config available and valid
[√] No orphan variables found
[√] No hardcoded variations found

Distribution:
• supervisor:
    [√] All remotes are reachable
    [√] All NoChange files are equal to local files
• students:
    [√] All remotes are reachable
    [√] All NoChange files are equal to local files
```

## divekit doctor list
List all available checks with short explanations:
```bash
$ divekit doctor list

You can call single checks or check groups by calling
`$ divekit doctor check <comma-separated-dot-notated-paths>`

Example:
`$ divekit doctor check system.token
[√] Token is valid and has the necessary permissions
`

system    - checks all children
  git     - checks if `git` is accessible
  npm     - checks if `npm` is accessible
  modules - checks if module dependencies are accessible
  token   - checks if the token is accessible and valid

origin                 - checks all children
  config               - checks if the origin config is valid
  orphan_variables     - checks if orphan variable names were found
  hardcoded_variations - checks if hardcoded variations were found

distribution           - checks all children
  <distribution_name>  - checks all children
    remotes_reachable  - checks if all configured remotes are reachable
    no_change_files    - checks if files not to be changed were changed  
```

## divekit doctor check
execute specific checks:
```bash
$ divekit doctor check system.token
[√] Token is valid and has the necessary permissions
```
```bash
$ divekit doctor check origin.config
[√] Config is available and valid
```
```bash
$ divekit doctor check system.token,origin.config
System:
  [√] Token is valid and has the necessary permissions
Origin:
  [√] Config is available and valid
```

## Comments

### 2024-09-12 Stefan, Fabian, Torben (in Person)
- move to another "error control" command?
- execute before other appropriate commands and possibly abort