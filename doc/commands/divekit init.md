# divekit init

> [!WARNING]
> Not yet implemented

Initializes a new Divekit origin repository by creating the necessary configuration files.  
(`npm init` and `git init` provide the expected functionality)

## Example
```bash
$ divekit init

This utility will walk you through creating the
necessary configuration files to turn this current
folder into an `origin` Divekit repository.

It only covers the most common items, and tries to
guess sensible defaults.

Press ^C at any time to quit.

? Repository name: ST2-{{now "2006"}}-{{uuid}}
? Distribution [milestone]:  test
? Use default structure [Y/n]: n
? Repository target   group id: 234567
? Repository test     group id: 345678
? Repository members csv [members.csv]:  ./../members.csv

Repository configuration created at `./.divekit_norepo/distribution/test/`
```
```bash
$ ls -a .divekit_norepo/distribution/test/
repositoryConfig.json
```

## Comments
- Merge with members for latecomers
- Also update overview (new members missing)
- Re-running ensures everything is in place
