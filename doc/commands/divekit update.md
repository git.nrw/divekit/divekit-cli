# divekit update

checks for a newer version and starts an update

## Example Process
```bash
$ divekit update -y

Checking for updates...
Current version: 0.0.1
Latest version: 0.0.2

Downloading update...
Applying update...
Update applied successfully

New version: 0.0.2
```
---
```bash
$ divekit update -y

Checking for updates...
Current version: 0.0.2
Latest version: 0.0.2

Already up to date
```