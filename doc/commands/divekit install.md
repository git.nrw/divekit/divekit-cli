# divekit install

Installs the divekit CLI and the required modules.

## Example

```bash
$ divekit install

Installing divekit in home directory...
[√] divekit installed in home directory
[√] divekit executable added to PATH
```

## Comments

### 2024-09-12 Stefan, Fabian, Torben (in person)

-   Possibly look into open source to see how others do it
-   Offer executables, `divekit install` copies the/an executable into the home directory and writes the path to the divekit executable in the PATH (and an update executable?).
-   `divekit install`, which copies divekit into the user directory and adds the divekit path to the PATH (and maybe already prepares all the `doctor` preparations)

