# Userdata

## Summary
1. Get usernames e.g. by exporting campusIDs from ILIAS
   - (create a CSV if it's not already in that format)
2. Import of CSV via `divekit config set repository.members --import <csv-path>`
   - [warn](#inside-origin) if `<csv-path>` is in origin repo
   - [disallow](#inside-origin-outisde-_norepo) `<csv-path>` is in origin, but not in a `*_norepo` folder
3. store `<csv-path>` in config
4. add uuids in CSV next to usernames (/groups)
   - (I kinda hate that, but I don't see any other way)
     - `~/.divekit` -> not shareable
     - `./.divekit_norepo/*/members.csv` -> shareable, but now it's always on gitlab
5. serve overview html and members csv via `divekit overview`
   - use UUIDs to map usernames in overview
6. use csv to map usernames to UUIDs in individualization files
   - remove "members" from `individual_repositories.json`

### Errors / Warnings
#### Inside Origin
if `<csv-path>` is inside origin repo flash warning:

> [!WARNING]
> It is not recommended to store userdata in the origin repository.
> NEVER store sensitive data in public repositories!

... but allow it anyway.

---

#### Inside Origin Outisde `*_norepo`

if `<csv-path>` is not inside a `*_norepo` folder, flash warning:

> [!CAUTION]
> You're not allowed to store your user list in the part of the origin that will be distributed to all members of that very list.  
> Please move your CSV file to a secure location.

## Latecomers
- Add latecomers via `divekit config set repository.members --add <username1,username2>` 
  - Add latecomer groups via  `--add-group (<group-name>:)<username1,username2> `
- Distribute latecomers via `divekit distribute (--latecomers)` 
  - (can we check if all repos are already distributed and add the missing ones?)


## Overview

`divekit overview` serves an overview of the distribution and the members of the distribution.
It uses the file from `Config::repository.members.csv` to display the members.
The HTML file contains the UUIDs of the members, which are used to map the usernames in the overview.

```go
indexPath := ".divekit_norepo/distribution/overview.html"
indexPath := fmt.Sprintf(".divekit_norepo/distribution/%s/overview.html", distroName)
csvPath := Config.Get("repository.members.csv")

// Serve index.html
http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
    http.ServeFile(w, r, indexPath)
})

// Serve .csv
http.HandleFunc("/members.csv", func(w http.ResponseWriter, r *http.Request) {
    http.ServeFile(w, r, csvPath)
})

fmt.Println("Serving on http://localhost:8080")
err := http.ListenAndServe(":8080", nil)
if err != nil {
    fmt.Println("Error starting server:", err)
    os.Exit(1)
}

openBrowser("http://localhost:8080")
```

## Individualization files
`individual_repositories.json` currently stores the "members" aswell.
This should be removed.  
If needed, the user information can be mapped via the csv file.

---

## Questions
- Use original CSV to store username+UUID pairs?
  - or create a new json file for that?
    - then we need to define where to store it by default
      - home directory?
      - in the origin repo? (shouldn't be the default)
      - define at `divekit init`?
- Do latecomers come one by one? Or in groups?
- Does the current overview create a link per group/repo (or per user)?
- Do latecomers sometimes need to be added to existing groups?
- From where do you currently access the overview file? (phone, computer, ...)
  - for what? (is the username integral if you're on your phone?)

## Comments
### 2024-10-09 Stefan, Konrad, Torben (Zoom)
- latecomers come in groups
- import via new export from ILIAS with all campusIDs