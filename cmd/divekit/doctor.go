package divekit

import (
	"divekit/internal/doctor"

	"github.com/spf13/cobra"
)

// doctorCmd represents the doctor command
var doctorCmd = &cobra.Command{
	Use:   "doctor",
	Short: "Checks the system for potential issues",
	Long:  `Checks the system for potential issues.`,
	RunE: func(cmd *cobra.Command, args []string) error {
		return runDoctor(cmd, args)
	},
}

func init() {
	RootCmd.AddCommand(doctorCmd)
}

func runDoctor(cmd *cobra.Command, args []string) error {
	if err := doctor.CheckTools(); err != nil {
		return err
	}
	if err := doctor.CheckGitLabToken(); err != nil {
		return err
	}
	if err := doctor.SetupDivekit(); err != nil {
		return err
	}
	return nil
}
