package divekit

import (
	"divekit/internal/initialize"

	"github.com/apex/log"
	"github.com/spf13/cobra"
)

var initCmd = &cobra.Command{
	Use:   "init",
	Short: "Initializes a new Divekit Origin Repository",
	Long:  `Initializes a new Divekit Origin Repository by creating the necessary configuration files.`,
	RunE: func(cmd *cobra.Command, args []string) error {
		return runInit(cmd, args)
	},
}

func init() {
	RootCmd.AddCommand(initCmd)
}

func runInit(cmd *cobra.Command, args []string) error {
	initialize.DisplayWelcomeMessage()

	existingConfig, err := initialize.LoadExistingConfiguration()
	if err != nil {
		return err
	}

	finalModel, err := initialize.RunInteractivePrompt(existingConfig)
	if err != nil {
		log.WithError(err).Fatal("Error running program")
		return err
	}

	config := initialize.UpdateConfigurationFromModel(existingConfig, finalModel)

	if err := initialize.SaveConfiguration(config, finalModel.Distribution.Value()); err != nil {
		return err
	}

	initialize.DisplaySuccessMessage(config, finalModel.Distribution.Value(), finalModel.ResolvedPath)
	return nil
}
