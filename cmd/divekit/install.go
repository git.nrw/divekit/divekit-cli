package divekit

import (
	"divekit/internal/install"

	"github.com/apex/log"
	"github.com/spf13/cobra"
)

var installCmd = &cobra.Command{
	Use:   "install",
	Short: "Install divekit to make it globally available",
	Long:  `Installs divekit by copying the executable to the appropriate location for your operating system.`,
	RunE: func(cmd *cobra.Command, args []string) error {
		return runInstall(cmd, args)
	},
}

func init() {
	RootCmd.AddCommand(installCmd)
}

func runInstall(cmd *cobra.Command, args []string) error {
	log.Info("Starting Divekit installation...")

	if err := install.SetupDivekitDirectories(); err != nil {
		return err
	}

	if err := install.CreateDefaultEnvFile(); err != nil {
		return err
	}

	targetPath, err := install.DetermineInstallationPath()
	if err != nil {
		return err
	}

	if err := install.InstallExecutable(targetPath); err != nil {
		return err
	}

	log.WithFields(log.Fields{
		"path": targetPath,
	}).Info("Divekit successfully installed")

	return nil
}
