package divekit

import (
	"divekit/internal"
	"divekit/internal/distribute"
	"os"

	"github.com/apex/log"
	"github.com/spf13/cobra"
	config "gitlab.com/git.nrw/divekit/modules/config-management"
	gladapter "gitlab.com/git.nrw/divekit/modules/gitlab-adapter"
)

var distributeCmd = &cobra.Command{
	Use:   "distribute",
	Short: "Distribute group projects",
	Long:  `Create projects from the current directory using the selected distribution configuration.`,
	RunE: func(cmd *cobra.Command, args []string) error {
		return runDistribute(cmd, args)
	},
}

func init() {
	distributeCmd.Flags().StringVarP(&distribute.TokenFlag, "token", "t", "", "GitLab token")
	distributeCmd.Flags().StringVarP(&distribute.BaseURLFlag, "remote", "r", "", "Remote repository URL (GitLab Instance)")
	distributeCmd.Flags().StringVarP(&distribute.DistributionFlag, "distribution", "d", "", "Distribution to use")
	RootCmd.AddCommand(distributeCmd)
}

func runDistribute(cmd *cobra.Command, args []string) error {
	log.Debug("Starting distribution process...")

	divekitHome, err := internal.GetDivekitHome()
	if err != nil {
		return err
	}

	workingDir, err := os.Getwd()
	if err != nil {
		return err
	}
	log.Debug("Working directory: " + workingDir)

	distribution, err := config.LoadDistributionConfig(workingDir, distribute.DistributionFlag)
	if err != nil {
		return err
	}

	token, baseURL, err := config.ResolveCredentials(distribution, distribute.TokenFlag, distribute.BaseURLFlag, divekitHome)
	if err != nil {
		return err
	}

	members, err := config.LoadMembers(distribution, divekitHome)
	if err != nil {
		return err
	}

	glClient, err := gladapter.NewGitLabClient(token, baseURL)
	if err != nil {
		return err
	}

	memberStatus, _, err := glClient.ValidateMembers(members)
	if err != nil {
		return err
	}

	if err := distribute.ShowAndConfirmPlan(distribution, baseURL, memberStatus); err != nil {
		return err
	}

	mappings, err := distribute.CreateAndSetupProjects(workingDir, distribution, token, baseURL, memberStatus)
	if err != nil {
		return err
	}

	if err := config.SaveRemotesMappings(workingDir, distribution.Name, mappings); err != nil {
		return err
	}

	log.Info("\nDistribution completed successfully")
	return nil
}
