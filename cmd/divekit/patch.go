package divekit

import (
	"divekit/internal"
	"divekit/internal/patch"
	"divekit/internal/utils/resource"
	"fmt"
	"os"

	"github.com/apex/log"
	"github.com/spf13/cobra"
	config "gitlab.com/git.nrw/divekit/modules/config-management"
	gladapter "gitlab.com/git.nrw/divekit/modules/gitlab-adapter"
)

var patchCmd = &cobra.Command{
	Use:   "patch",
	Short: "Apply a patch to all repos",
	Long:  `Patch one or several files in all the repos of a certain distribution of the origin repo`,
	RunE:  runPatch,
}

func init() {
	log.Debug("patch.init()")
	patch.InitFlags()
	patchCmd.Flags().StringVarP(&patch.DistributionFlag, "distribution", "d", "",
		"name of the repo-distribution to patch")
	RootCmd.AddCommand(patchCmd)
}

func runPatch(cmd *cobra.Command, args []string) error {
	log.Debug("patch.runPatch()")

	if len(args) == 0 {
		return fmt.Errorf("no files specified to patch")
	}

	divekitHome, err := internal.GetDivekitHome()
	if err != nil {
		return err
	}

	workingDir, err := os.Getwd()
	if err != nil {
		return err
	}
	log.Debug("Working directory: " + workingDir)

	files := args
	if len(files) == 0 {
		return fmt.Errorf("no files specified to patch")
	}

	distribution, err := config.LoadDistributionConfig(workingDir, patch.DistributionFlag)
	if err != nil {
		return err
	}
	log.Debug("Distribution: " + distribution.Name)

	remotesConfig, err := config.LoadRemotes(workingDir, distribution)
	if err != nil {
		return err
	}

	token, baseURL, err := config.ResolveCredentials(distribution, patch.TokenFlag, patch.BaseURLFlag, divekitHome)
	if err != nil {
		return err
	}

	glClient, err := gladapter.NewGitLabClient(token, baseURL)
	if err != nil {
		return err
	}

	remoteProjectIDs := []int{}
	for _, remote := range remotesConfig.Remotes {
		remoteProjectIDs = append(remoteProjectIDs, remote.ProjectID)
	}

	// check if all files exist on the remotes (currently only one remote will be checked (third parameter: checkAllProjects = false))
	remoteFilesStatus, err := glClient.ValidateFiles(remoteProjectIDs, files, false)
	if err != nil {
		return err
	}

	// check if all files exist locally
	localFilesStatus, err := resource.ValidateFiles(distribution, files)
	if err != nil {
		return err
	}

	// show what files will be created, deleted, or updated on which remotes (baseURL, groupID, ...)
	if err := patch.ShowAndConfirmPlan(distribution, baseURL, remoteFilesStatus, localFilesStatus); err != nil {
		return err
	}

	// Convert local file status to gitlab adapter format
	adaptedLocalStatus := make(map[string]*gladapter.FileStatus)
	for path, status := range localFilesStatus {
		adaptedLocalStatus[path] = &gladapter.FileStatus{
			Path:   path,
			Exists: status.Exists,
		}
	}

	// upload files
	if err := glClient.UploadFiles(remoteProjectIDs, remoteFilesStatus, adaptedLocalStatus); err != nil {
		return err
	}

	log.Info("\nPatch completed successfully")
	return nil
}
