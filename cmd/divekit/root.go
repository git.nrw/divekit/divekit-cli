package divekit

import (
	"divekit/internal"
	"divekit/internal/utils/logUtils"

	"github.com/apex/log"
	"github.com/spf13/cobra"
)

var (
	LogLevelFlag    string
	DivekitHomeFlag string

	RootCmd = NewRootCmd()
)

func init() {
	log.Debug("divekit.init()")
	SetCmdFlags(RootCmd)
}

func NewRootCmd() *cobra.Command {
	return &cobra.Command{
		Use:   "divekit",
		Short: "divekit helps to create and distribute individualized repos for software engineering exercises",
		Long: `Divekit has been developed at TH Köln by the ArchiLab team (www.archi-lab.io) as
universal tool to design, individualize, distribute, assess, patch, and evaluate
realistic software engineering exercises as Git repos.`,
		PersistentPreRunE: persistentPreRun,
	}
}

func SetCmdFlags(cmd *cobra.Command) {
	log.Debug("divekit.SetCmdFlags()")
	cmd.PersistentFlags().StringVarP(&LogLevelFlag, "loglevel", "l", "info",
		"log level (warn, info, debug, error)")
	cmd.PersistentFlags().StringVarP(&DivekitHomeFlag, "home", "m", "",
		"home directory of all the Divekit repos")
}

func persistentPreRun(cmd *cobra.Command, args []string) error {
	log.Debug("divekit.persistentPreRun()")
	if err := logUtils.DefineLoggingLevel(LogLevelFlag); err != nil {
		log.Errorf("Could not define the logging level flag:", err)
		return err
	}

	if err := internal.InitDivekitHomeDir(DivekitHomeFlag); err != nil {
		log.Errorf("Could not initialize the divekit home flag:", err)
		return err
	}

	return nil
}

func Execute() error {
	log.Debug("divekit.Execute()")
	return RootCmd.Execute()
}

type InvalidArgsError struct {
	Msg string
}

func (e *InvalidArgsError) Error() string {
	return e.Msg
}
